package org.nrg.xnat.dicom.test.unit

import org.apache.commons.lang3.StringUtils
import org.dcm4che3.data.PersonName
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals
import static org.testng.AssertJUnit.assertNull

class TestPersonNames {

    @Test
    void testParsing() {
        final String romanLast = 'Yamada'
        final String romanFirst = 'Tarou'
        final String alphabetic = StringUtils.joinWith('^', romanLast, romanFirst)
        final String ideographic = '山田^太郎'
        final String phonetic = 'やまだ^たろう'
        final String fullNameRepresentation = StringUtils.joinWith('=', alphabetic, ideographic, phonetic)

        final PersonName personName = new PersonName(fullNameRepresentation)
        assertEquals(alphabetic, personName.toString(PersonName.Group.Alphabetic, true))
        assertEquals(ideographic, personName.toString(PersonName.Group.Ideographic, true))
        assertEquals(phonetic, personName.toString(PersonName.Group.Phonetic, true))
        assertEquals(romanLast, personName.get(PersonName.Group.Alphabetic, PersonName.Component.FamilyName))
        assertEquals(romanFirst, personName.get(PersonName.Group.Alphabetic, PersonName.Component.GivenName))
        assertNull(personName.get(PersonName.Group.Alphabetic, PersonName.Component.MiddleName))
    }

    @Test
    void testParsingSingleGroup() {
        final String lastName = 'Last-Name'
        final String firstName = 'First'
        final String middleName = 'My Middle Name'
        final String prefix = 'Dr.'
        final String suffix = 'Esq.'
        final String fullNameRepresentation = StringUtils.joinWith('^', lastName, firstName, middleName, prefix, suffix)

        final PersonName personName = new PersonName(fullNameRepresentation)
        assertEquals(fullNameRepresentation, personName.toString())
        assertEquals(fullNameRepresentation, personName.toString(PersonName.Group.Alphabetic, true))
        assertEquals(lastName, personName.get(PersonName.Group.Alphabetic, PersonName.Component.FamilyName))
        assertEquals(firstName, personName.get(PersonName.Group.Alphabetic, PersonName.Component.GivenName))
        assertEquals(middleName, personName.get(PersonName.Group.Alphabetic, PersonName.Component.MiddleName))
        assertEquals(prefix, personName.get(PersonName.Group.Alphabetic, PersonName.Component.NamePrefix))
        assertEquals(suffix, personName.get(PersonName.Group.Alphabetic, PersonName.Component.NameSuffix))
    }

    @Test
    void testDelimiterJumbling() {
        final String suffix = 'Jr.'
        final String phonetic = "^^^^${suffix}"
        final String fullString = " ==${phonetic}"

        final PersonName personName = new PersonName(fullString)
        assertEquals('', personName.toString(PersonName.Group.Alphabetic, true))
        assertEquals('', personName.toString(PersonName.Group.Ideographic, true))
        assertNull(personName.get(PersonName.Group.Phonetic, PersonName.Component.FamilyName))
        assertNull(personName.get(PersonName.Group.Phonetic, PersonName.Component.GivenName))
        assertNull(personName.get(PersonName.Group.Phonetic, PersonName.Component.MiddleName))
        assertNull(personName.get(PersonName.Group.Phonetic, PersonName.Component.NamePrefix))
        assertEquals(suffix, personName.get(PersonName.Group.Phonetic, PersonName.Component.NameSuffix))
        assertEquals(phonetic, personName.toString(PersonName.Group.Phonetic, true))
        assertEquals(StringUtils.stripStart(fullString, ' '), personName.toString())
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    void testInvalidStringTooManyComponents() {
        new PersonName('NAME^NAME^NAME^NAME^NAME^NAME')
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    void testInvalidStringTooManyGroups() {
        new PersonName('NAME=NAME=NAME=NAME')
    }

}
