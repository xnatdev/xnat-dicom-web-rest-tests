package org.nrg.xnat.dicom.test.integration

import org.dcm4che3.data.Keyword
import org.nrg.testing.annotations.TestRequires
import org.nrg.testing.enums.TestData
import org.nrg.xnat.dicom.enums.WadoRequestType
import org.nrg.xnat.dicom.wado.WadoTestSpec
import org.nrg.xnat.dicom.web.BaseDicomWebTest
import org.nrg.xnat.dicom.web.DicomWebTestInstance
import org.nrg.xnat.dicom.web.DicomWebTestSeries
import org.nrg.xnat.dicom.web.DicomWebTestStudy
import org.nrg.xnat.dicom.qido.QidoTestSpec
import org.nrg.xnat.dicom.enums.QidoRequestContentType
import org.nrg.xnat.enums.Accessibility
import org.nrg.xnat.enums.Gender
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.scans.MRScan
import org.nrg.xnat.pogo.experiments.sessions.CTSession
import org.nrg.xnat.pogo.experiments.sessions.MRSession
import org.nrg.xnat.pogo.experiments.sessions.PETMRSession
import org.nrg.xnat.pogo.experiments.sessions.PETSession
import org.nrg.xnat.pogo.extensions.SimpleResourceFileExtension
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension
import org.nrg.xnat.pogo.resources.ResourceFile
import org.nrg.xnat.pogo.resources.ScanResource
import org.testng.annotations.Test

import static org.dcm4che3.data.Tag.*
import static org.nrg.xnat.dicom.web.DicomWebTestStudy.*
import static org.nrg.xnat.dicom.web.DicomWebTestSeries.*
import static org.nrg.xnat.dicom.web.DicomWebTestInstance.*
import static org.nrg.xnat.dicom.enums.QidoRequestContentType.*
import static org.nrg.xnat.dicom.enums.QidoRequestType.*
import static org.nrg.xnat.dicom.enums.WadoRequestContentType.*

@TestRequires(
        data = [
            TestData.DICOM_WEB_PETCT1,
            TestData.DICOM_WEB_PETCT2_PET,
            TestData.DICOM_WEB_PETCT2_CT,
            TestData.DICOM_WEB_CTRT1,
            TestData.DICOM_WEB_CTRT2,
            TestData.DICOM_WEB_PETMR1,
            TestData.DICOM_WEB_PETMR2_MR,
            TestData.DICOM_WEB_PETMR2_PT,
            TestData.DICOM_WEB_PETMR3,
            TestData.DICOM_WEB_MRPR,
            TestData.DICOM_WEB_MULTIFRAME_CT,
            TestData.NIFTI_FILE,
            TestData.DICOM_WEB_MISSINGNO_MR,
            TestData.DICOM_WEB_MG
        ]
)
class TestsWithComplexData extends BaseDicomWebTest {

    @SuppressWarnings("GroovyResultOfObjectAllocationIgnored")
    @Override
    List<Project> defineClassProjects() {
        final Project mainProject = new Project().accessibility(Accessibility.PROTECTED).addCollaborator(mainUser)
        final Project noAccessProject = new Project().accessibility(Accessibility.PROTECTED)

        final Subject petctSubject = new Subject(mainProject).gender(null).dob(PETCT1.birthDateObject())
        final Subject ctWithRtSubject1 = new Subject(mainProject).gender(Gender.UNKNOWN)
        final Subject ctWithRtSubject2 = new Subject(mainProject).gender(Gender.UNKNOWN)
        final Subject petMrSubject = new Subject(mainProject).gender(Gender.UNKNOWN).dob(PETMR2.birthDateObject())
        final Subject mrWithPrSubject = new Subject(mainProject).gender(Gender.UNKNOWN).dob(MRPR.birthDateObject())
        final Subject multiframeCtSubject = new Subject(mainProject).gender(Gender.MALE).dob(MULTIFRAME_CT.birthDateObject())
        final Subject missingnoSubject = new Subject(mainProject).dob(MISSINGNO_MR.birthDateObject())
        final Subject mgSubject = new Subject(mainProject).gender(Gender.FEMALE)

        final PETSession petct1 = new PETSession(mainProject, petctSubject)
        final PETSession petct2Pet = new PETSession(mainProject, petctSubject)
        final CTSession petct2Ct = new CTSession(mainProject, petctSubject)
        final CTSession ctWithRt1 = new CTSession(mainProject, ctWithRtSubject1)
        final CTSession ctWithRt2 = new CTSession(mainProject, ctWithRtSubject2)
        final PETMRSession petmr1 = new PETMRSession(mainProject, petMrSubject)
        final PETSession petmr2Pet = new PETSession(mainProject, petMrSubject)
        final MRSession petmr2Mr = new MRSession(mainProject, petMrSubject)
        final CTSession multiframeCt1 = new CTSession(mainProject, multiframeCtSubject)
        final MRSession missingnoMr = new MRSession(mainProject, missingnoSubject)
        final ImagingSession mg = new ImagingSession(mainProject, mgSubject)
        final MRSession mrWithPr1 = new MRSession(mainProject, mrWithPrSubject)

        final MRScan scanUnderMrpr = new MRScan(mrWithPr1, '601')
        new ScanResource(mainProject, mrWithPrSubject, mrWithPr1, scanUnderMrpr, 'NIFTI').addResourceFile(new ResourceFile().unzip(true).extension(new SimpleResourceFileExtension(TestData.NIFTI_FILE.toFile())))
        final MRScan newScanUnderMrpr = new MRScan(mrWithPr1, '901')
        new ScanResource(mainProject, mrWithPrSubject, mrWithPr1, newScanUnderMrpr, 'NIFTI').addResourceFile(new ResourceFile().unzip(true).extension(new SimpleResourceFileExtension(TestData.NIFTI_FILE.toFile())))
        new MRScan(missingnoMr, Long.toString(MISSINGNO_MR_MR_3.seriesNumber)).type('OTHER TYPE')
        addImportExtension(petct1, PETCT1)
        addImportExtension(petct2Pet, PETCT2)
        new SessionImportExtension(petct2Ct, TestData.DICOM_WEB_PETCT2_CT.toFile())
        addImportExtension(ctWithRt1, CTRT1)
        addImportExtension(ctWithRt2, CTRT2)
        addImportExtension(petmr1, PETMR1)
        addImportExtension(petmr2Pet, PETMR2)
        addImportExtension(multiframeCt1, MULTIFRAME_CT)
        addImportExtension(missingnoMr, MISSINGNO_MR)
        addImportExtension(mg, MG)
        new SessionImportExtension(petmr2Mr, TestData.DICOM_WEB_PETMR2_MR.toFile())
        addImportExtension(mrWithPr1, MRPR)

        final Subject niftiSubject = new Subject(mainProject)
        final MRSession niftiSession = new MRSession(mainProject, niftiSubject).date(MRPR.studyDateObject())
        final MRScan niftiScan = new MRScan(niftiSession, '1')
        niftiScan.addResource(new ScanResource(mainProject, niftiSubject, niftiSession, niftiScan, 'NIFTI').addResourceFile(new ResourceFile().unzip(true).extension(new SimpleResourceFileExtension(TestData.NIFTI_FILE.toFile()))))

        final Subject inaccessibleSubject = new Subject(noAccessProject)
        addImportExtension(new PETMRSession(noAccessProject, inaccessibleSubject), PETMR3)
        [mainProject, noAccessProject]
    }

    /**
     * Study-level QIDO-RS tests
     */

    @Test
    void testModalitiesInStudyComplexSessionsJsonQuery() {
        performModalitiesInStudyComplexSessionsQueryTest(JSON)
    }

    @Test
    void testModalitiesInStudyComplexSessionsXmlQuery() {
        performModalitiesInStudyComplexSessionsQueryTest(XML)
    }

    @Test
    void testModalitiesInStudyWildcardingJsonQuery() {
        performModalitiesInStudyWildcardingQueryTest(JSON)
    }

    @Test
    void testModalitiesInStudyWildcardingXmlQuery() {
        performModalitiesInStudyWildcardingQueryTest(XML)
    }

    @Test
    void testStudyIDJsonQuery() {
        performStudyIDQueryTest(JSON)
    }

    @Test
    void testStudyIDXmlQuery() {
        performStudyIDQueryTest(XML)
    }

    @Test
    void testStudyIDWildcardingJsonQuery() {
        performStudyIDWildcardingQueryTest(JSON)
    }

    @Test
    void testStudyIDWildcardingXmlQuery() {
        performStudyIDWildcardingQueryTest(XML)
    }

    @Test
    void testStudyIDUniversalMatchingWithValuesJsonQuery() {
        performStudyIDUniversalMatchingQueryTest(JSON)
    }

    @Test
    void testStudyIDUniversalMatchingWithValuesXmlQuery() {
        performStudyIDUniversalMatchingQueryTest(XML)
    }

    @Test
    void testStudyIDCaseSensitivityJsonQuery() {
        performStudyIDCaseSensitivityTest(JSON)
    }

    @Test
    void testStudyIDCaseSensitivityXmlQuery() {
        performStudyIDCaseSensitivityTest(XML)
    }

    @Test
    void testQuerylessStudyJsonQuery() {
        performQuerylessStudyQueryTest(JSON)
    }

    @Test
    void testQuerylessStudyXmlQuery() {
        performQuerylessStudyQueryTest(XML)
    }

    @Test
    void testPartialPatientNameWithoutWildcardJsonQuery() {
        performPartialPatientNameQueryWithoutWildcardTest(JSON)
    }

    @Test
    void testPartialPatientNameWithoutWildcardXmlQuery() {
        performPartialPatientNameQueryWithoutWildcardTest(XML)
    }

    @Test
    void testPartialPatientNameWithWildcardJsonQuery() {
        performPartialPatientNameQueryWithWildcardTest(JSON)
    }

    @Test
    void testPartialPatientNameWithWildcardXmlQuery() {
        performPartialPatientNameQueryWithWildcardTest(XML)
    }

    @Test
    void testUnsupportedStudyParamsWithoutWarningsJsonQuery() {
        performUnsupportedStudyParamsWithoutWarningsQueryTest(JSON)
    }

    @Test
    void testUnsupportedStudyParamsWithoutWarningsXmlQuery() {
        performUnsupportedStudyParamsWithoutWarningsQueryTest(XML)
    }

    @Test
    void testIncludefieldAllJsonQuery() {
        performIncludefieldAllQueryTest(JSON)
    }

    @Test
    void testIncludefieldAllXmlQuery() {
        performIncludefieldAllQueryTest(XML)
    }

    @Test
    void testStudyDateRangeModalitiesInStudyCombinedJsonQuery() {
        performStudyDateRangeModalitiesInStudyCombinedTest(JSON)
    }

    @Test
    void testStudyDateRangeModalitiesInStudyCombinedXmlQuery() {
        performStudyDateRangeModalitiesInStudyCombinedTest(XML)
    }

    @Test
    void testPatientNameModalitiesInStudyWildcardCombinedJsonQuery() {
        performPatientNameModalitiesInStudyWildcardCombinedTest(JSON)
    }

    @Test
    void testPatientNameModalitiesInStudyWildcardCombinedXmlQuery() {
        performPatientNameModalitiesInStudyWildcardCombinedTest(XML)
    }

    @Test
    void testBadStudyDateJsonQuery() {
        performBadStudyDateQueryTest(JSON)
    }

    @Test
    void testBadStudyDateXmlQuery() {
        performBadStudyDateQueryTest(XML)
    }

    /**
     * Series-level QIDO-RS tests
     */

    @Test
    void testQuerylessSeriesJsonQuery() {
        performQuerylessSeriesQueryTest(JSON)
    }

    @Test
    void testQuerylessSeriesXmlQuery() {
        performQuerylessSeriesQueryTest(XML)
    }

    @Test
    void testScopedQuerylessSplitStudySeriesJsonQuery() {
        performScopedQuerylessSeriesSplitStudyQueryTest(JSON)
    }

    @Test
    void testScopedQuerylessSplitStudySeriesXmlQuery() {
        performScopedQuerylessSeriesSplitStudyQueryTest(XML)
    }

    @Test
    void testModalityJsonQuery() {
        performModalityQueryTest(JSON)
    }

    @Test
    void testModalityXmlQuery() {
        performModalityQueryTest(XML)
    }

    @Test
    void testModalityWildcardJsonQuery() {
        performModalityWildcardQueryTest(JSON)
    }

    @Test
    void testModalityWildcardXmlQuery() {
        performModalityWildcardQueryTest(XML)
    }

    @Test
    void testScopedModalityJsonQuery() {
        performScopedModalityQueryTest(JSON)
    }

    @Test
    void testScopedModalityXmlQuery() {
        performScopedModalityQueryTest(XML)
    }

    @Test
    void testScopedModalityWildcardJsonQuery() {
        performScopedModalityWildcardQueryTest(JSON)
    }

    @Test
    void testScopedModalityWildcardXmlQuery() {
        performScopedModalityWildcardQueryTest(XML)
    }

    @Test
    void testSeriesInstanceUIDJsonQuery() {
        performSeriesInstanceUIDQueryTest(JSON)
    }

    @Test
    void testSeriesInstanceUIDXmlQuery() {
        performSeriesInstanceUIDQueryTest(XML)
    }

    @Test
    void testScopedSeriesInstanceUIDJsonQuery() {
        performScopedSeriesInstanceUIDQueryTest(JSON)
    }

    @Test
    void testScopedSeriesInstanceUIDXmlQuery() {
        performScopedSeriesInstanceUIDQueryTest(XML)
    }

    @Test
    void testSeriesInstanceUIDListJsonQuery() {
        performSeriesInstanceUIDListQueryTest(JSON)
    }

    @Test
    void testSeriesInstanceUIDListXmlQuery() {
        performSeriesInstanceUIDListQueryTest(XML)
    }

    @Test
    void testScopedSeriesInstanceUIDListJsonQuery() {
        performScopedSeriesInstanceUIDListQueryTest(JSON)
    }

    @Test
    void testScopedSeriesInstanceUIDListXmlQuery() {
        performScopedSeriesInstanceUIDListQueryTest(XML)
    }

    @Test
    void testSeriesNumberJsonQuery() {
        performSeriesNumberQueryTest(JSON)
    }

    @Test
    void testSeriesNumberXmlQuery() {
        performSeriesNumberQueryTest(XML)
    }

    @Test
    void testScopedSeriesNumberJsonQuery() {
        performScopedSeriesNumberQueryTest(JSON)
    }

    @Test
    void testScopedSeriesNumberXmlQuery() {
        performScopedSeriesNumberQueryTest(XML)
    }
    
    @Test
    void testPerformedProcedureStepStartDateJsonQuery() {
        performPerformedProcedureStepStartDateQueryTest(JSON)
    }

    @Test
    void testPerformedProcedureStepStartDateXmlQuery() {
        performPerformedProcedureStepStartDateQueryTest(XML)
    }
    
    @Test
    void testPerformedProcedureStepStartDateRangeJsonQuery() {
        performPerformedProcedureStepStartDateRangeQueryTest(JSON)
    }

    @Test
    void testPerformedProcedureStepStartDateRangeXmlQuery() {
        performPerformedProcedureStepStartDateRangeQueryTest(XML)
    }
    
    @Test
    void testPerformedProcedureStepStartDateRangeOpenEndpointJsonQuery() {
        performPerformedProcedureStepStartDateRangeOpenEndpointQueryTest(JSON)
    }

    @Test
    void testPerformedProcedureStepStartDateRangeOpenEndpointXmlQuery() {
        performPerformedProcedureStepStartDateRangeOpenEndpointQueryTest(XML)
    }

    @Test
    void testScopedPerformedProcedureStepStartDateJsonQuery() {
        performScopedPerformedProcedureStepStartDateQueryTest(JSON)
    }

    @Test
    void testScopedPerformedProcedureStepStartDateXmlQuery() {
        performScopedPerformedProcedureStepStartDateQueryTest(XML)
    }

    @Test
    void testScopedPerformedProcedureStepStartDateRangeJsonQuery() {
        performScopedPerformedProcedureStepStartDateRangeQueryTest(JSON)
    }

    @Test
    void testScopedPerformedProcedureStepStartDateRangeXmlQuery() {
        performScopedPerformedProcedureStepStartDateRangeQueryTest(XML)
    }

    @Test
    void testScopedPerformedProcedureStepStartDateRangeOpenEndpointJsonQuery() {
        performScopedPerformedProcedureStepStartDateRangeOpenEndpointQueryTest(JSON)
    }

    @Test
    void testScopedPerformedProcedureStepStartDateRangeOpenEndpointXmlQuery() {
        performScopedPerformedProcedureStepStartDateRangeOpenEndpointQueryTest(XML)
    }
    
    @Test
    void testPerformedProcedureStepStartTimeJsonQuery() {
        performPerformedProcedureStepStartTimeQueryTest(JSON)
    }

    @Test
    void testPerformedProcedureStepStartTimeXmlQuery() {
        performPerformedProcedureStepStartTimeQueryTest(XML)
    }

    @Test
    void testPerformedProcedureStepStartTimeRangeJsonQuery() {
        performPerformedProcedureStepStartTimeRangeQueryTest(JSON)
    }

    @Test
    void testPerformedProcedureStepStartTimeRangeXmlQuery() {
        performPerformedProcedureStepStartTimeRangeQueryTest(XML)
    }

    @Test
    void testPerformedProcedureStepStartTimeRangeOpenEndpointJsonQuery() {
        performPerformedProcedureStepStartTimeRangeOpenEndpointQueryTest(JSON)
    }

    @Test
    void testPerformedProcedureStepStartTimeRangeOpenEndpointXmlQuery() {
        performPerformedProcedureStepStartTimeRangeOpenEndpointQueryTest(XML)
    }

    @Test
    void testScopedPerformedProcedureStepStartTimeJsonQuery() {
        performScopedPerformedProcedureStepStartTimeQueryTest(JSON)
    }

    @Test
    void testScopedPerformedProcedureStepStartTimeXmlQuery() {
        performScopedPerformedProcedureStepStartTimeQueryTest(XML)
    }

    @Test
    void testScopedPerformedProcedureStepStartTimeRangeJsonQuery() {
        performScopedPerformedProcedureStepStartTimeRangeQueryTest(JSON)
    }

    @Test
    void testScopedPerformedProcedureStepStartTimeRangeXmlQuery() {
        performScopedPerformedProcedureStepStartTimeRangeQueryTest(XML)
    }

    @Test
    void testScopedPerformedProcedureStepStartTimeRangeOpenEndpointJsonQuery() {
        performScopedPerformedProcedureStepStartTimeRangeOpenEndpointQueryTest(JSON)
    }

    @Test
    void testScopedPerformedProcedureStepStartTimeRangeOpenEndpointXmlQuery() {
        performScopedPerformedProcedureStepStartTimeRangeOpenEndpointQueryTest(XML)
    }

    @Test
    void testPerformedProcedureStepStartDatetimeCombinedJsonQuery() {
        performPerformedProcedureStepStartDatetimeCombinedQueryTest(JSON)
    }

    @Test
    void testPerformedProcedureStepStartDatetimeCombinedXmlQuery() {
        performPerformedProcedureStepStartDatetimeCombinedQueryTest(XML)
    }

    @Test
    void testScopedPerformedProcedureStepStartDatetimeCombinedJsonQuery() {
        performScopedPerformedProcedureStepStartDatetimeCombinedQueryTest(JSON)
    }

    @Test
    void testScopedPerformedProcedureStepStartDatetimeCombinedXmlQuery() {
        performScopedPerformedProcedureStepStartDatetimeCombinedQueryTest(XML)
    }

    @Test
    void testUnsupportedSeriesParamsWithoutWarningsJsonQuery() {
        performUnsupportedSeriesParamsWithoutWarningsQueryTest(JSON)
    }

    @Test
    void testUnsupportedSeriesParamsWithoutWarningsXmlQuery() {
        performUnsupportedSeriesParamsWithoutWarningsQueryTest(XML)
    }

    @Test
    void testBadPerformedProcedureStepStartJsonQuery() {
        performBadPerformedProcedureStepStartQueryTest(JSON)
    }

    @Test
    void testBadPerformedProcedureStepStartXmlQuery() {
        performBadPerformedProcedureStepStartQueryTest(XML)
    }

    @Test
    void testSeriesNumberModalityCombinedJsonQuery() {
        performSeriesNumberModalityCombinedTest(JSON)
    }

    @Test
    void testSeriesNumberModalityCombinedXmlQuery() {
        performSeriesNumberModalityCombinedTest(XML)
    }

    @Test
    void testAllSeriesParamsCombinedJsonQuery() {
        performAllSeriesParamsCombinedTest(JSON, false)
    }

    @Test
    void testAllSeriesParamsCombinedXmlQuery() {
        performAllSeriesParamsCombinedTest(XML, false)
    }

    @Test
    void testScopedAllSeriesParamsCombinedJsonQuery() {
        performAllSeriesParamsCombinedTest(JSON, true)
    }

    @Test
    void testScopedAllSeriesParamsCombinedXmlQuery() {
        performAllSeriesParamsCombinedTest(XML, true)
    }

    /**
     * WADO-RS tests
     */

    @Test
    void testStudyRetrieve() {
        performBasicWadoStudyTest([PETCT1, MG, MISSINGNO_MR, MRPR])
    }

    @Test
    void testSplitStudyRetrieve() {
        performBasicWadoStudyTest([PETCT2])
    }

    @Test
    void testMultiframeStudyRetrieve() {
        performBasicWadoStudyTest([MULTIFRAME_CT])
    }

    @Test
    void testSeriesRetrieve() {
        performBasicWadoSeriesTest([PETCT1, MG, MISSINGNO_MR, MRPR])
    }

    @Test
    void testSplitStudySeriesRetrieve() {
        performBasicWadoSeriesTest([PETCT2])
    }

    @Test
    void testMultiframeSeriesRetrieve() {
        performBasicWadoSeriesTest([MULTIFRAME_CT])
    }

    @Test
    void testInstanceRetrieve() {
        performBasicWadoInstanceTest([PETCT1, MRPR, MISSINGNO_MR, MG])
    }

    @Test
    void testSplitStudyInstanceRetrieve() {
        performBasicWadoInstanceTest([PETCT2])
    }

    @Test
    void testMultiframeInstanceRetrieve() {
        performBasicWadoInstanceTest([MULTIFRAME_CT])
    }

    @Test
    void testUnknownStudyRetrieve() {
        execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID('1.1.2.3.5.8.13.21.34.55.89.144').user(mainUser).contentType(DICOM).expectedStatusCode(404))
    }

    @Test
    void testUnknownSeriesRetrieve() {
        execute(new WadoTestSpec(WadoRequestType.SERIES).studyInstanceUID('1.1.2.3.5.8.13.21.34.55.89.144').seriesInstanceUID('2.4.6.8.10').user(mainUser).contentType(DICOM).expectedStatusCode(404))
        execute(new WadoTestSpec(WadoRequestType.SERIES).studyInstanceUID(MULTIFRAME_CT).seriesInstanceUID('2.4.6.8.10').user(mainUser).contentType(DICOM).expectedStatusCode(404))
    }

    @Test
    void testUnknownInstanceRetrieve() {
        execute(new WadoTestSpec(WadoRequestType.INSTANCE).studyInstanceUID('1.1.2.3.5.8.13.21.34.55.89.144').seriesInstanceUID('2.4.6.8.10').sopInstanceUID('1.1.1.1.1.1.1.1.1').user(mainUser).contentType(DICOM).expectedStatusCode(404))
        execute(new WadoTestSpec(WadoRequestType.INSTANCE).studyInstanceUID(MULTIFRAME_CT).seriesInstanceUID('2.4.6.8.10').sopInstanceUID('1.1.1.1.1.1.1.1.1').user(mainUser).contentType(DICOM).expectedStatusCode(404))
        execute(new WadoTestSpec(WadoRequestType.INSTANCE).studyInstanceUID(MULTIFRAME_CT).seriesInstanceUID(MULTIFRAME_CT_CT_1).sopInstanceUID('1.1.1.1.1.1.1.1.1').user(mainUser).contentType(DICOM).expectedStatusCode(404))
    }

    @Test
    void testUnsupportedBulkDataStudyRetrieve() {
        execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID(MULTIFRAME_CT).user(mainUser).contentType(BULK_DATA).expectedStatusCode(406))
    }

    @Test
    void testUnsupportedBulkDataSeriesRetrieve() {
        execute(new WadoTestSpec(WadoRequestType.SERIES).seriesInstanceUID(MULTIFRAME_CT_CT_2).user(mainUser).contentType(BULK_DATA).expectedStatusCode(406))
    }

    @Test
    void testUnsupportedBulkDataInstanceRetrieve() {
        execute(new WadoTestSpec(WadoRequestType.INSTANCE).sopInstanceUID(MULTIFRAME_CT_CT_2_INSTANCE).user(mainUser).contentType(BULK_DATA).expectedStatusCode(406))
    }

    @Test
    void testUnsupportedPixelDataStudyRetrieve() {
        [JPEG, JPX, MP4].each { format ->
            execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID(MG).user(mainUser).contentType(format).expectedStatusCode(406))
        }
    }

    @Test
    void testUnsupportedPixelDataSeriesRetrieve() {
        [JPEG, JPX, MP4].each { format ->
            execute(new WadoTestSpec(WadoRequestType.SERIES).seriesInstanceUID(MG_MG_3).user(mainUser).contentType(format).expectedStatusCode(406))
        }
    }

    @Test
    void testUnsupportedPixelDataInstanceRetrieve() {
        [JPEG, JPX, MP4].each { format ->
            execute(new WadoTestSpec(WadoRequestType.INSTANCE).sopInstanceUID(MG_MG_2_INSTANCE).user(mainUser).contentType(format).expectedStatusCode(406))
        }
    }

    private void performModalitiesInStudyComplexSessionsQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(ModalitiesInStudy, 'MR').expectedResult([PETMR1, PETMR2, MRPR, MISSINGNO_MR]))
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(ModalitiesInStudy, 'PT').expectedResult([PETCT1, PETCT2, PETMR1, PETMR2]))
    }

    private void performModalitiesInStudyWildcardingQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(ModalitiesInStudy, 'RT*').expectedResult([CTRT1, CTRT2]))
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(ModalitiesInStudy, 'RTST*').expectedResult([CTRT1, CTRT2]))
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(ModalitiesInStudy, 'RTD*').expectedResult(CTRT1))
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(ModalitiesInStudy, 'RTM*'))
    }

    private void performStudyIDQueryTest(QidoRequestContentType contentType) {
        [PETCT2, PETMR1].each { study ->
            execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(StudyID, study.studyID).expectedResult(study))
        }
    }

    private void performStudyIDWildcardingQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(StudyID, '0522c0001*').expectedResult([PETCT1, PETCT2, CTRT1]))
    }

    private void performStudyIDUniversalMatchingQueryTest(QidoRequestContentType contentType) { // PS3.4 C.2.2.2.3
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(StudyID, '*').expectedResult([PETCT1, PETCT2, CTRT1, CTRT2, PETMR1, PETMR2, MRPR, MULTIFRAME_CT, MISSINGNO_MR, MG]))
    }

    private void performStudyIDCaseSensitivityTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(StudyID, PETMR1.studyID.toLowerCase()))
    }

    private void performQuerylessStudyQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser)) // See #17 : https://wiki.xnat.org/display/XNATDev/DICOM+RS?focusedCommentId=53674438#comment-53674438
    }

    private void performPartialPatientNameQueryWithoutWildcardTest(QidoRequestContentType contentType) {
        ['WATERMELON', 'watermelon'].each { partialName ->
            execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(PatientName, partialName))
        }
    }

    private void performPartialPatientNameQueryWithWildcardTest(QidoRequestContentType contentType) {
        ['WATERMELON*', 'watermelon*'].each { partialName ->
            execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(PatientName, partialName).expectedResult(MRPR))
        }
    }

    private void performUnsupportedStudyParamsWithoutWarningsQueryTest(QidoRequestContentType contentType) {
        final Map queryMap = [
                limit : 1,
                offset : 2,
                (DicomUtils.intToSimpleHeaderString(PatientName)) : PETCT1.patientName,
                (DicomUtils.intToSimpleHeaderString(ReferringPhysicianName)) : 'DOCTOR',
                ("${DicomUtils.intToSimpleHeaderString(OtherPatientIDsSequence)}.${Keyword.valueOf(PatientID)}".toString()) : '11235813',
                includefield : (DicomUtils.intToSimpleHeaderString(EthnicGroup))
        ]
        final List<DicomWebTestStudy> expectedStudies = [PETCT1, PETCT2, CTRT1]
        final QidoTestSpec test = new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).queryParams(queryMap).expectedResult(expectedStudies)

        execute(test)
        queryMap.remove(DicomUtils.intToSimpleHeaderString(ReferringPhysicianName))
        queryMap[Keyword.valueOf(ReferringPhysicianName)] = 'DOCTOR'
        execute(test)
    }

    private void performIncludefieldAllQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).queryParams([(DicomUtils.intToSimpleHeaderString(PatientName)) : PETCT1.patientName, includefield : 'all']).expectedResult([PETCT1, PETCT2, CTRT1]))
    }

    private void performStudyDateRangeModalitiesInStudyCombinedTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).queryParams([(DicomUtils.intToSimpleHeaderString(StudyDate)) : '19990831-', (Keyword.valueOf(ModalitiesInStudy)) : 'CT']).expectedResult([PETCT2, CTRT1, MULTIFRAME_CT]))
    }

    private void performPatientNameModalitiesInStudyWildcardCombinedTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).queryParams([(DicomUtils.intToSimpleHeaderString(ModalitiesInStudy)) : 'RT*', (Keyword.valueOf(PatientName)) : '0522c0001']).expectedResult(CTRT1))
    }

    private void performBadStudyDateQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(mainUser).simpleQuery(StudyDate, 'DATE').expectedStatusCode(400))
    }

    private void performQuerylessSeriesQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser)) // See #17 : https://wiki.xnat.org/display/XNATDev/DICOM+RS?focusedCommentId=53674438#comment-53674438
    }

    private void performScopedQuerylessSeriesSplitStudyQueryTest(QidoRequestContentType contentType) {
        [PETCT1, PETCT2, PETMR2].each { study ->
            execute(new QidoTestSpec(SERIES).studyInstanceUID(study).contentType(contentType).user(mainUser).expectedResult(study.series))
        }
    }

    private void performModalityQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(Modality, 'CT').expectedResult([PETCT1_CT_2, PETCT2_CT_2, CTRT1_CT_1, CTRT2_CT_1] + MULTIFRAME_CT.series))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(Modality, 'RTSTRUCT').expectedResult([CTRT1_RTSTRUCT_2, CTRT2_RTSTRUCT_2]))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(Modality, 'PR').expectedResult([MRPR_PR_0_A, MRPR_PR_0_B]))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(Modality, 'PT').expectedResult([PETCT1_PT_605, PETCT1_PT_606, PETCT2_PT_605, PETCT2_PT_606, PETMR1_PT_1, PETMR2_PT_4]))
    }

    private void performModalityWildcardQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(Modality, 'P*').expectedResult([PETCT1_PT_605, PETCT1_PT_606, PETCT2_PT_605, PETCT2_PT_606, PETMR1_PT_1, PETMR2_PT_4, MRPR_PR_0_A, MRPR_PR_0_B]))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(Modality, 'RT*').expectedResult([CTRT1_RTSTRUCT_2, CTRT1_RTPLAN_3, CTRT1_RTDOSE_4, CTRT2_RTSTRUCT_2]))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(Modality, 'X*'))
    }

    private void performScopedModalityQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETCT1).contentType(contentType).user(mainUser).simpleQuery(Modality, 'CT').expectedResult([PETCT1_CT_2]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETCT2).contentType(contentType).user(mainUser).simpleQuery(Modality, 'PT').expectedResult([PETCT2_PT_605, PETCT2_PT_606]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MRPR).contentType(contentType).user(mainUser).simpleQuery(Modality, 'PR').expectedResult([MRPR_PR_0_A, MRPR_PR_0_B]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MRPR).contentType(contentType).user(mainUser).simpleQuery(Modality, 'PT'))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MULTIFRAME_CT).contentType(contentType).user(mainUser).simpleQuery(Modality, 'CT').expectedResult(MULTIFRAME_CT.series))
    }

    private void performScopedModalityWildcardQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(CTRT1).contentType(contentType).user(mainUser).simpleQuery(Modality, 'RT*').expectedResult([CTRT1_RTSTRUCT_2, CTRT1_RTPLAN_3, CTRT1_RTDOSE_4]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(CTRT1).contentType(contentType).user(mainUser).simpleQuery(Modality, 'M*'))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETMR1).contentType(contentType).user(mainUser).simpleQuery(Modality, 'M*').expectedResult([PETMR1_MR_5436027, PETMR1_MR_5442056]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETMR1).contentType(contentType).user(mainUser).simpleQuery(Modality, '*').expectedResult(PETMR1.series))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETMR2).contentType(contentType).user(mainUser).simpleQuery(Modality, 'M*').expectedResult([PETMR2_MR_5610035, PETMR2_MR_5616046]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETMR1).contentType(contentType).user(mainUser).simpleQuery(Modality, '*').expectedResult(PETMR2.series))
    }

    private void performSeriesInstanceUIDQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(SeriesInstanceUID, MISSINGNO_MR_MR_EMPTY.seriesInstanceUID).expectedResult(MISSINGNO_MR_MR_EMPTY))
    }

    private void performScopedSeriesInstanceUIDQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MISSINGNO_MR).contentType(contentType).user(mainUser).simpleQuery(SeriesInstanceUID, MISSINGNO_MR_MR_EMPTY.seriesInstanceUID).expectedResult(MISSINGNO_MR_MR_EMPTY))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MISSINGNO_MR).contentType(contentType).user(mainUser).simpleQuery(SeriesInstanceUID, PETMR2_MR_5616046.seriesInstanceUID))
    }

    private void performSeriesInstanceUIDListQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(SeriesInstanceUID, [MRPR_PR_0_A.seriesInstanceUID, '8.6.7.5.3.0.9', MRPR_MR_601.seriesInstanceUID]).expectedResult([MRPR_PR_0_A, MRPR_MR_601]))
    }

    private void performScopedSeriesInstanceUIDListQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MRPR).contentType(contentType).user(mainUser).simpleQuery(SeriesInstanceUID, [MRPR_PR_0_A.seriesInstanceUID, '8.6.7.5.3.0.9', MRPR_MR_601.seriesInstanceUID, PETCT2_PT_606.seriesInstanceUID]).expectedResult([MRPR_PR_0_A, MRPR_MR_601]))
    }

    private void performSeriesNumberQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(SeriesNumber, '1').expectedResult([CTRT1_CT_1, CTRT2_CT_1, PETMR1_PT_1, MULTIFRAME_CT_CT_1, MISSINGNO_MR_MR_1, MG_MG_1]))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(SeriesNumber, '0').expectedResult([MRPR_MR_0, MRPR_PR_0_A, MRPR_PR_0_B]))
    }

    private void performScopedSeriesNumberQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(CTRT1).contentType(contentType).user(mainUser).simpleQuery(SeriesNumber, '1').expectedResult(CTRT1_CT_1))
    }

    private void performPerformedProcedureStepStartDateQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '20061219').expectedResult(MULTIFRAME_CT.series + MG_MG_2))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '20100503').expectedResult(MRPR_PR_0_B))
    }

    private void performPerformedProcedureStepStartDateRangeQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '20000907-20061220').expectedResult(PETMR1.series + PETMR2.series + MULTIFRAME_CT.series + [MG_MG_1, MG_MG_2]))
    }

    private void performPerformedProcedureStepStartDateRangeOpenEndpointQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '-19990831').expectedResult(PETCT1.series + CTRT1.series))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '20061221-').expectedResult(MRPR.series + MG_MG_3))
    }

    private void performScopedPerformedProcedureStepStartDateQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MRPR).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '20100430').expectedResult(MRPR.series - MRPR_PR_0_B))
    }

    private void performScopedPerformedProcedureStepStartDateRangeQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MRPR).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '20100428-20100502').expectedResult(MRPR.series - MRPR_PR_0_B))
    }

    private void performScopedPerformedProcedureStepStartDateRangeOpenEndpointQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MG).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '-20061220').expectedResult([MG_MG_1, MG_MG_2]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MG).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartDate, '20061220-').expectedResult([MG_MG_1, MG_MG_3]))
    }

    private void performPerformedProcedureStepStartTimeQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartTime, '112011').expectedResult([PETMR1_PT_1, MG_MG_1]))
    }

    private void performPerformedProcedureStepStartTimeRangeQueryTest(QidoRequestContentType contentType) {
        ['09-13', '090000-130000', '090000.123-130000.456'].each { queryVariant ->
            execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartTime, queryVariant).expectedResult(PETMR1.series + MULTIFRAME_CT.series + [PETMR2_MR_5610035, PETMR2_MR_5616046, MRPR_MR_0, MRPR_PR_0_B, MG_MG_1]))
        }
    }

    private void performPerformedProcedureStepStartTimeRangeOpenEndpointQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartTime, '-1023').expectedResult([MRPR_PR_0_B, MG_MG_2, MG_MG_3]))
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartTime, '1307-').expectedResult(PETCT1.series + PETCT2.series + [PETMR2_PT_4, MRPR_PR_0_A, MRPR_MR_701]))
    }

    private void performScopedPerformedProcedureStepStartTimeQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETMR1).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartTime, '112011').expectedResult(PETMR1_PT_1))
    }

    private void performScopedPerformedProcedureStepStartTimeRangeQueryTest(QidoRequestContentType contentType) {
        ['09-13', '090000-130000', '090000.123-130000.456'].each { queryVariant ->
            execute(new QidoTestSpec(SERIES).studyInstanceUID(MRPR).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartTime, queryVariant).expectedResult([MRPR_MR_0, MRPR_PR_0_B]))
        }
    }

    private void performScopedPerformedProcedureStepStartTimeRangeOpenEndpointQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MG).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartTime, '-1023').expectedResult([MG_MG_2, MG_MG_3]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETMR2).contentType(contentType).user(mainUser).simpleQuery(PerformedProcedureStepStartTime, '1307-').expectedResult(PETMR2_PT_4))
    }

    private void performPerformedProcedureStepStartDatetimeCombinedQueryTest(QidoRequestContentType contentType) {
        execute(
                new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).querySearchParams([(PerformedProcedureStepStartDate) : '19990831-20061219', (PerformedProcedureStepStartTime) : '030303-110929']).
                expectedResult(PETCT2.series + CTRT2.series + PETMR1.series + PETMR2.series + [MULTIFRAME_CT_CT_2, MULTIFRAME_CT_CT_3, MULTIFRAME_CT_CT_4, MG_MG_2])
        )
    }

    private void performScopedPerformedProcedureStepStartDatetimeCombinedQueryTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(MG).contentType(contentType).user(mainUser).querySearchParams([(PerformedProcedureStepStartDate) : '19990831-20061219', (PerformedProcedureStepStartTime) : '030303-110929']).expectedResult(MG_MG_2))
    }

    private void performUnsupportedSeriesParamsWithoutWarningsQueryTest(QidoRequestContentType contentType) {
        final String sequenceParam1 = "${DicomUtils.intToSimpleHeaderString(RequestAttributesSequence)}.${Keyword.valueOf(ScheduledProcedureStepID)}"
        final String sequenceParam2 = "${Keyword.valueOf(RequestAttributesSequence)}.${DicomUtils.intToSimpleHeaderString(RequestedProcedureID)}"
        final Map queryMap = [
                limit : 1,
                offset : 2,
                (DicomUtils.intToSimpleHeaderString(SeriesInstanceUID)) : PETMR2_MR_5616046.seriesInstanceUID,
                (sequenceParam1) : 5,
                includefield : (DicomUtils.intToSimpleHeaderString(ProtocolName))
        ]
        final QidoTestSpec test = new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).queryParams(queryMap).expectedResult(PETMR2_MR_5616046)
        execute(test)
        queryMap.remove(sequenceParam1)
        queryMap[sequenceParam2] = '12345'
        execute(test)
    }

    private void performBadPerformedProcedureStepStartQueryTest(QidoRequestContentType contentType) {
        [PerformedProcedureStepStartDate, PerformedProcedureStepStartTime].each { procedure ->
            execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).simpleQuery(procedure, 'DATETIME').expectedStatusCode(400))
        }
    }

    private void performSeriesNumberModalityCombinedTest(QidoRequestContentType contentType) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(mainUser).querySearchParams([(SeriesNumber) : 1, (Modality) : 'CT']).expectedResult([CTRT1_CT_1, CTRT2_CT_1, MULTIFRAME_CT_CT_1]))
    }

    private void performAllSeriesParamsCombinedTest(QidoRequestContentType contentType, boolean scoped) {
        final QidoTestSpec testSpec = new QidoTestSpec(SERIES).contentType(contentType).user(mainUser)
        testSpec.querySearchParams([(Modality) : 'C*', (SeriesInstanceUID) : MULTIFRAME_CT_CT_1.seriesInstanceUID, (SeriesNumber) : 1, (PerformedProcedureStepStartDate) : '20061219', (PerformedProcedureStepStartTime) : '11-12'])
        if (scoped) testSpec.studyInstanceUID(MULTIFRAME_CT)
        execute(testSpec)
    }

    private void performBasicWadoStudyTest(Collection<DicomWebTestStudy> studies) {
        studies.each { study ->
            execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID(study).user(mainUser).contentType(DICOM).expectedResult(study))
        }
    }

    private void performBasicWadoSeriesTest(Collection<DicomWebTestStudy> baseStudies) {
        (baseStudies.series.flatten() as List<DicomWebTestSeries>).each { series ->
            execute(new WadoTestSpec(WadoRequestType.SERIES).seriesInstanceUID(series).user(mainUser).contentType(DICOM).expectedResult(series))
        }
    }

    private void performBasicWadoInstanceTest(Collection<DicomWebTestStudy> baseStudies) {
        // TODO: why do I need to cast all over the place here??
        baseStudies.each { study ->
            study.series.each { series ->
                ((series as DicomWebTestSeries).knownInstances as Collection<DicomWebTestInstance>).each { instance ->
                    execute(new WadoTestSpec(WadoRequestType.INSTANCE).sopInstanceUID(instance).user(mainUser).contentType(DICOM).expectedResult(instance))
                }
            }
        }
    }

}
