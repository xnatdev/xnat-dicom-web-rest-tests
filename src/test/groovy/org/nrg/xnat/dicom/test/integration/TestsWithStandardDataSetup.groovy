package org.nrg.xnat.dicom.test.integration

import org.nrg.testing.annotations.TestRequires
import org.nrg.testing.enums.TestData
import org.nrg.xnat.dicom.web.DicomWebTestSeries
import org.nrg.xnat.dicom.web.DicomWebTestStudy
import org.nrg.xnat.dicom.web.BaseDicomWebTest
import org.nrg.xnat.dicom.qido.QidoTestSpec
import org.nrg.xnat.dicom.enums.QidoRequestContentType
import org.nrg.xnat.enums.Accessibility
import org.nrg.xnat.enums.Gender
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.sessions.CTSession
import org.nrg.xnat.pogo.experiments.sessions.MRSession
import org.nrg.xnat.pogo.users.User
import org.testng.annotations.Test

import static org.nrg.xnat.dicom.web.DicomWebTestStudy.*
import static org.nrg.xnat.dicom.enums.QidoRequestContentType.*
import static org.nrg.xnat.dicom.enums.QidoRequestType.*
import static org.dcm4che3.data.Tag.*

@TestRequires(
        users = 3,
        data = [
            TestData.DICOM_WEB_CT1,
            TestData.DICOM_WEB_CT2,
            TestData.DICOM_WEB_CT3,
            TestData.DICOM_WEB_CT4,
            TestData.DICOM_WEB_CT5,
            TestData.DICOM_WEB_CT6,
            TestData.DICOM_WEB_CT7,
            TestData.DICOM_WEB_CT8
        ]
)
class TestsWithStandardDataSetup extends BaseDicomWebTest {

    private User user1, user2, user3

    @Override
    List<Project> defineClassProjects() {
        user1 = getGenericUser()
        user2 = getGenericUser()
        user3 = getGenericUser()

        final Project publicProject = new Project().accessibility(Accessibility.PUBLIC)
        final Project privateProject2 = new Project().accessibility(Accessibility.PRIVATE).addCollaborator(user1).addMember(user2)
        final Project privateProject3 = new Project().accessibility(Accessibility.PRIVATE).addOwner(user1).addCollaborator(user3)

        final Subject subject1_1 = new Subject(publicProject).gender(Gender.MALE)
        final Subject subject1_2 = new Subject(publicProject).gender(Gender.MALE)
        final Subject subject2_1 = new Subject(privateProject2).gender(Gender.MALE)
        final Subject subject2_2 = new Subject(privateProject2).gender(Gender.FEMALE)
        final Subject subject3_1 = new Subject(privateProject3).gender(Gender.FEMALE)
        final Subject subject3_2 = new Subject(privateProject3).gender(Gender.FEMALE)

        final MRSession mr1 = new MRSession(publicProject, subject1_1)    // readable by: user1, user2, user3
        final CTSession ct1 = new CTSession(publicProject, subject1_1)    // readable by: user1, user2, user3
        final CTSession ct2 = new CTSession(publicProject, subject1_2)    // readable by: user1, user2, user3
        final CTSession ct3 = new CTSession(publicProject, subject1_2)    // readable by: user1, user2, user3
        final MRSession mr2 = new MRSession(privateProject2, subject2_1)  // readable by: user1, user2
        final CTSession ct4 = new CTSession(privateProject2, subject2_1)  // readable by: user1, user2
        final CTSession ct5 = new CTSession(privateProject2, subject2_2)  // readable by: user1, user2
        final CTSession ct6 = new CTSession(privateProject2, subject2_2)  // readable by: user1, user2
        final MRSession mr3 = new MRSession(privateProject3, subject3_1)  // readable by: user1, user3
        final CTSession ct7 = new CTSession(privateProject3, subject3_1)  // readable by: user1, user3
        final MRSession mr4 = new MRSession(privateProject3, subject3_2)  // readable by: user1, user3
        final CTSession ct8 = new CTSession(privateProject3, subject3_2)  // readable by: user1, user3
        addImportExtension(ct1, CT1)
        addImportExtension(ct2, CT2)
        addImportExtension(ct3, CT3)
        addImportExtension(ct4, CT4)
        addImportExtension(ct5, CT5)
        addImportExtension(ct6, CT6)
        addImportExtension(ct7, CT7)
        addImportExtension(ct8, CT8)
        addImportExtension(mr1, MR1)
        addImportExtension(mr2, MR2)
        addImportExtension(mr3, MR3)
        addImportExtension(mr4, MR4)
        // TODO: uncomment next line when XNAT-5411 is fixed
        // new PETSession(publicProject, subject1_1).date(LocalDate.parse('20170101', TimeUtils.DICOM_DA_FORMAT))
        [publicProject, privateProject2, privateProject3]
    }

    /**
     * Study-level QIDO-RS tests
     */

    @Test
    void testStudyDateLiteralQueryUser1Json() {
        performStudyDateQueryTest(JSON, user1, [CT1, CT2, CT4, CT7, MR1])
    }

    @Test
    void testStudyDateLiteralQueryUser2Json() {
        performStudyDateQueryTest(JSON, user2, [CT1, CT2, CT4, MR1])
    }

    @Test
    void testStudyDateLiteralQueryUser3Json() {
        performStudyDateQueryTest(JSON, user3, [CT1, CT2, CT7, MR1])
    }

    @Test
    void testStudyDateLiteralQueryUser1Xml() {
        performStudyDateQueryTest(XML, user1, [CT1, CT2, CT4, CT7, MR1])
    }

    @Test
    void testStudyDateLiteralQueryUser2Xml() {
        performStudyDateQueryTest(XML, user2, [CT1, CT2, CT4, MR1])
    }

    @Test
    void testStudyDateLiteralQueryUser3Xml() {
        performStudyDateQueryTest(XML, user3, [CT1, CT2, CT7, MR1])
    }

    @Test
    void testStudyDateRangeQueryUser1Json() {
        performStudyDateRangeQueryTest(JSON, user1, [CT3, CT5, CT6, MR2, MR3])
    }

    @Test
    void testStudyDateRangeQueryUser2Json() {
        performStudyDateRangeQueryTest(JSON, user2, [CT3, CT5, CT6, MR2])
    }

    @Test
    void testStudyDateRangeQueryUser3Json() {
        performStudyDateRangeQueryTest(JSON, user3, [CT3, MR3])
    }

    @Test
    void testStudyDateRangeQueryUser1Xml() {
        performStudyDateRangeQueryTest(XML, user1, [CT3, CT5, CT6, MR2, MR3])
    }

    @Test
    void testStudyDateRangeQueryUser2Xml() {
        performStudyDateRangeQueryTest(XML, user2, [CT3, CT5, CT6, MR2])
    }

    @Test
    void testStudyDateRangeQueryUser3Xml() {
        performStudyDateRangeQueryTest(XML, user3, [CT3, MR3])
    }

    @Test
    void testStudyDateRangeOpenEndpointQueryUser1Json() {
        performStudyDateRangeOpenEndpointQueryTest(JSON, user1, [CT1, CT2, CT3, CT4, CT5, CT7, MR1, MR2, MR3], [CT3, CT5, CT6, CT8, MR2, MR3, MR4])
    }

    @Test
    void testStudyDateRangeOpenEndpointQueryUser2Json() {
        performStudyDateRangeOpenEndpointQueryTest(JSON, user2, [CT1, CT2, CT3, CT4, CT5, MR1, MR2], [CT3, CT5, CT6, MR2])
    }

    @Test
    void testStudyDateRangeOpenEndpointQueryUser3Json() {
        performStudyDateRangeOpenEndpointQueryTest(JSON, user3, [CT1, CT2, CT3, CT7, MR1, MR3], [CT3, CT8, MR3, MR4])
    }

    @Test
    void testStudyDateRangeOpenEndpointQueryUser1Xml() {
        performStudyDateRangeOpenEndpointQueryTest(XML, user1, [CT1, CT2, CT3, CT4, CT5, CT7, MR1, MR2, MR3], [CT3, CT5, CT6, CT8, MR2, MR3, MR4])
    }

    @Test
    void testStudyDateRangeOpenEndpointQueryUser2Xml() {
        performStudyDateRangeOpenEndpointQueryTest(XML, user2, [CT1, CT2, CT3, CT4, CT5, MR1, MR2], [CT3, CT5, CT6, MR2])
    }

    @Test
    void testStudyDateRangeOpenEndpointQueryUser3Xml() {
        performStudyDateRangeOpenEndpointQueryTest(XML, user3, [CT1, CT2, CT3, CT7, MR1, MR3], [CT3, CT8, MR3, MR4])
    }

    @Test
    void testStudyTimeLiteralQueryUser1Json() {
        performStudyTimeQueryTest(JSON, user1)
    }

    @Test
    void testStudyTimeLiteralQueryUser2Json() {
        performStudyTimeQueryTest(JSON, user2)
    }

    @Test
    void testStudyTimeLiteralQueryUser3Json() {
        performStudyTimeQueryTest(JSON, user3)
    }

    @Test
    void testStudyTimeLiteralQueryUser1Xml() {
        performStudyTimeQueryTest(XML, user1)
    }

    @Test
    void testStudyTimeLiteralQueryUser2Xml() {
        performStudyTimeQueryTest(XML, user2)
    }

    @Test
    void testStudyTimeLiteralQueryUser3Xml() {
        performStudyTimeQueryTest(XML, user3)
    }

    @Test
    void testStudyTimeRangeQueryUser1Json() {
        performStudyTimeRangeQueryTest(JSON, user1, [CT1, CT3, CT7, CT8, MR1, MR3])
    }

    @Test
    void testStudyTimeRangeQueryUser2Json() {
        performStudyTimeRangeQueryTest(JSON, user2, [CT1, CT3, MR1])
    }

    @Test
    void testStudyTimeRangeQueryUser3Json() {
        performStudyTimeRangeQueryTest(JSON, user3, [CT1, CT3, CT7, CT8, MR1, MR3])
    }

    @Test
    void testStudyTimeRangeQueryUser1Xml() {
        performStudyTimeRangeQueryTest(XML, user1, [CT1, CT3, CT7, CT8, MR1, MR3])
    }

    @Test
    void testStudyTimeRangeQueryUser2Xml() {
        performStudyTimeRangeQueryTest(XML, user2, [CT1, CT3, MR1])
    }

    @Test
    void testStudyTimeRangeQueryUser3Xml() {
        performStudyTimeRangeQueryTest(XML, user3, [CT1, CT3, CT7, CT8, MR1, MR3])
    }

    @Test
    void testStudyTimeOpenFrontRangeQueryUser1Json() {
        performStudyTimeOpenEndpointQueryTest(JSON, user1, true, [CT7, MR1, MR2, MR3, MR4])
    }

    @Test
    void testStudyTimeOpenFrontRangeQueryUser2Json() {
        performStudyTimeOpenEndpointQueryTest(JSON, user2, true, [MR1, MR2])
    }

    @Test
    void testStudyTimeOpenFrontRangeQueryUser3Json() {
        performStudyTimeOpenEndpointQueryTest(JSON, user3, true, [CT7, MR1, MR3, MR4])
    }

    @Test
    void testStudyTimeOpenFrontRangeQueryUser1Xml() {
        performStudyTimeOpenEndpointQueryTest(XML, user1, true, [CT7, MR1, MR2, MR3, MR4])
    }

    @Test
    void testStudyTimeOpenFrontRangeQueryUser2Xml() {
        performStudyTimeOpenEndpointQueryTest(XML, user2, true, [MR1, MR2])
    }

    @Test
    void testStudyTimeOpenFrontRangeQueryUser3Xml() {
        performStudyTimeOpenEndpointQueryTest(XML, user3, true, [CT7, MR1, MR3, MR4])
    }

    @Test
    void testStudyTimeOpenEndRangeQueryUser1Json() {
        performStudyTimeOpenEndpointQueryTest(JSON, user1, false, [CT1, CT2, CT3, CT4, CT5, CT6, CT8])
    }

    @Test
    void testStudyTimeOpenEndRangeQueryUser2Json() {
        performStudyTimeOpenEndpointQueryTest(JSON, user2, false, [CT1, CT2, CT3, CT4, CT5, CT6])
    }

    @Test
    void testStudyTimeOpenEndRangeQueryUser3Json() {
        performStudyTimeOpenEndpointQueryTest(JSON, user3, false, [CT1, CT2, CT3, CT8])
    }

    @Test
    void testStudyTimeOpenEndRangeQueryUser1Xml() {
        performStudyTimeOpenEndpointQueryTest(XML, user1, false, [CT1, CT2, CT3, CT4, CT5, CT6, CT8])
    }

    @Test
    void testStudyTimeOpenEndRangeQueryUser2Xml() {
        performStudyTimeOpenEndpointQueryTest(XML, user2, false, [CT1, CT2, CT3, CT4, CT5, CT6])
    }

    @Test
    void testStudyTimeOpenEndRangeQueryUser3Xml() {
        performStudyTimeOpenEndpointQueryTest(XML, user3, false, [CT1, CT2, CT3, CT8])
    }
    
    @Test
    void testStudyDateStudyTimeCombinedMatchingUser1Json() {
        performStudyDateStudyTimeCombinedMatchingQueryTest(JSON, user1, [CT3, MR3])
    }

    @Test
    void testStudyDateStudyTimeCombinedMatchingUser2Json() {
        performStudyDateStudyTimeCombinedMatchingQueryTest(JSON, user2, [CT3])
    }

    @Test
    void testStudyDateStudyTimeCombinedMatchingUser3Json() {
        performStudyDateStudyTimeCombinedMatchingQueryTest(JSON, user3, [CT3, MR3])
    }

    @Test
    void testStudyDateStudyTimeCombinedMatchingUser1Xml() {
        performStudyDateStudyTimeCombinedMatchingQueryTest(XML, user1, [CT3, MR3])
    }

    @Test
    void testStudyDateStudyTimeCombinedMatchingUser2Xml() {
        performStudyDateStudyTimeCombinedMatchingQueryTest(XML, user2, [CT3])
    }

    @Test
    void testStudyDateStudyTimeCombinedMatchingUser3Xml() {
        performStudyDateStudyTimeCombinedMatchingQueryTest(XML, user3, [CT3, MR3])
    }
    
    @Test
    void testAccessionNumberQueryUser1Json() {
        performAccessionNumberQueryTest(JSON, user1)
    }

    @Test
    void testAccessionNumberQueryUser2Json() {
        performAccessionNumberQueryTest(JSON, user2)
    }

    @Test
    void testAccessionNumberQueryUser3Json() {
        performAccessionNumberQueryTest(JSON, user3)
    }

    @Test
    void testAccessionNumberQueryUser1Xml() {
        performAccessionNumberQueryTest(XML, user1)
    }

    @Test
    void testAccessionNumberQueryUser2Xml() {
        performAccessionNumberQueryTest(XML, user2)
    }

    @Test
    void testAccessionNumberQueryUser3Xml() {
        performAccessionNumberQueryTest(XML, user3)
    }

    @Test
    void testAccessionNumberAsteriskQueryUser1Json() {
        performAccessionNumberAsteriskQueryTest(JSON, user1, [CT4, MR1, MR4])
    }

    @Test
    void testAccessionNumberAsteriskQueryUser2Json() {
        performAccessionNumberAsteriskQueryTest(JSON, user2, [CT4, MR1])
    }

    @Test
    void testAccessionNumberAsteriskQueryUser3Json() {
        performAccessionNumberAsteriskQueryTest(JSON, user3, [MR1, MR4])
    }

    @Test
    void testAccessionNumberAsteriskQueryUser1Xml() {
        performAccessionNumberAsteriskQueryTest(XML, user1, [CT4, MR1, MR4])
    }

    @Test
    void testAccessionNumberAsteriskQueryUser2Xml() {
        performAccessionNumberAsteriskQueryTest(XML, user2, [CT4, MR1])
    }

    @Test
    void testAccessionNumberAsteriskQueryUser3Xml() {
        performAccessionNumberAsteriskQueryTest(XML, user3, [MR1, MR4])
    }

    @Test
    void testAccessionNumberQuestionMarkQueryUser1Json() {
        performAccessionNumberQuestionMarkQueryTest(JSON, user1)
    }

    @Test
    void testAccessionNumberQuestionMarkQueryUser2Json() {
        performAccessionNumberQuestionMarkQueryTest(JSON, user2)
    }

    @Test
    void testAccessionNumberQuestionMarkQueryUser3Json() {
        performAccessionNumberQuestionMarkQueryTest(JSON, user3)
    }

    @Test
    void testAccessionNumberQuestionMarkQueryUser1Xml() {
        performAccessionNumberQuestionMarkQueryTest(XML, user1)
    }

    @Test
    void testAccessionNumberQuestionMarkQueryUser2Xml() {
        performAccessionNumberQuestionMarkQueryTest(XML, user2)
    }

    @Test
    void testAccessionNumberQuestionMarkQueryUser3Xml() {
        performAccessionNumberQuestionMarkQueryTest(XML, user3)
    }

    @Test
    void testModalitiesInStudyQueryUser1Json() {
        performModalitiesInStudyQueryTest(JSON, user1, [CT1, CT2, CT3, CT4, CT5, CT6, CT7, CT8])
    }

    @Test
    void testModalitiesInStudyQueryUser2Json() {
        performModalitiesInStudyQueryTest(JSON, user2, [CT1, CT2, CT3, CT4, CT5, CT6])
    }

    @Test
    void testModalitiesInStudyQueryUser3Json() {
        performModalitiesInStudyQueryTest(JSON, user3, [CT1, CT2, CT3, CT7, CT8])
    }

    @Test
    void testModalitiesInStudyQueryUser1Xml() {
        performModalitiesInStudyQueryTest(XML, user1, [CT1, CT2, CT3, CT4, CT5, CT6, CT7, CT8])
    }

    @Test
    void testModalitiesInStudyQueryUser2Xml() {
        performModalitiesInStudyQueryTest(XML, user2, [CT1, CT2, CT3, CT4, CT5, CT6])
    }

    @Test
    void testModalitiesInStudyQueryUser3Xml() {
        performModalitiesInStudyQueryTest(XML, user3, [CT1, CT2, CT3, CT7, CT8])
    }
    
    @Test
    void testPatientNameQueryUser1Json() {
        performPatientNameQueryTest(JSON, user1)
    }

    @Test
    void testPatientNameQueryUser2Json() {
        performPatientNameQueryTest(JSON, user2)
    }

    @Test
    void testPatientNameQueryUser3Json() {
        performPatientNameQueryTest(JSON, user3)
    }

    @Test
    void testPatientNameQueryUser1Xml() {
        performPatientNameQueryTest(XML, user1)
    }

    @Test
    void testPatientNameQueryUser2Xml() {
        performPatientNameQueryTest(XML, user2)
    }

    @Test
    void testPatientNameQueryUser3Xml() {
        performPatientNameQueryTest(XML, user3)
    }

    @Test
    void testPatientNameAsteriskQueryUser1Json() {
        performPatientNameAsteriskQueryTest(JSON, user1, [CT2, CT3, CT5, CT6, CT8, MR4])
    }

    @Test
    void testPatientNameAsteriskQueryUser2Json() {
        performPatientNameAsteriskQueryTest(JSON, user2, [CT2, CT3, CT5, CT6])
    }

    @Test
    void testPatientNameAsteriskQueryUser3Json() {
        performPatientNameAsteriskQueryTest(JSON, user3, [CT2, CT3, CT8, MR4])
    }

    @Test
    void testPatientNameAsteriskQueryUser1Xml() {
        performPatientNameAsteriskQueryTest(XML, user1, [CT2, CT3, CT5, CT6, CT8, MR4])
    }

    @Test
    void testPatientNameAsteriskQueryUser2Xml() {
        performPatientNameAsteriskQueryTest(XML, user2, [CT2, CT3, CT5, CT6])
    }

    @Test
    void testPatientNameAsteriskQueryUser3Xml() {
        performPatientNameAsteriskQueryTest(XML, user3, [CT2, CT3, CT8, MR4])
    }

    @Test
    void testPatientNameQuestionMarkQueryUser1Json() {
        performPatientNameQuestionMarkQueryTest(JSON, user1)
    }

    @Test
    void testPatientNameQuestionMarkQueryUser2Json() {
        performPatientNameQuestionMarkQueryTest(JSON, user2)
    }

    @Test
    void testPatientNameQuestionMarkQueryUser3Json() {
        performPatientNameQuestionMarkQueryTest(JSON, user3)
    }

    @Test
    void testPatientNameQuestionMarkQueryUser1Xml() {
        performPatientNameQuestionMarkQueryTest(XML, user1)
    }

    @Test
    void testPatientNameQuestionMarkQueryUser2Xml() {
        performPatientNameQuestionMarkQueryTest(XML, user2)
    }

    @Test
    void testPatientNameQuestionMarkQueryUser3Xml() {
        performPatientNameQuestionMarkQueryTest(XML, user3)
    }
    
    @Test
    void testPatientNameCaseInsensitiveQueryUser1Json() {
        performPatientNameCaseInsensitiveQueryTest(JSON, user1)
    }

    @Test
    void testPatientNameCaseInsensitiveQueryUser2Json() {
        performPatientNameCaseInsensitiveQueryTest(JSON, user2)
    }

    @Test
    void testPatientNameCaseInsensitiveQueryUser3Json() {
        performPatientNameCaseInsensitiveQueryTest(JSON, user3)
    }
    
    @Test
    void testPatientNameCaseInsensitiveQueryUser1Xml() {
        performPatientNameCaseInsensitiveQueryTest(XML, user1)
    }

    @Test
    void testPatientNameCaseInsensitiveQueryUser2Xml() {
        performPatientNameCaseInsensitiveQueryTest(XML, user2)
    }

    @Test
    void testPatientNameCaseInsensitiveQueryUser3Xml() {
        performPatientNameCaseInsensitiveQueryTest(XML, user3)
    }

    @Test
    void testPatientIDQueryUser1Json() {
        performPatientIDQueryTest(JSON, user1)
    }

    @Test
    void testPatientIDQueryUser2Json() {
        performPatientIDQueryTest(JSON, user2)
    }

    @Test
    void testPatientIDQueryUser3Json() {
        performPatientIDQueryTest(JSON, user3)
    }

    @Test
    void testPatientIDQueryUser1Xml() {
        performPatientIDQueryTest(XML, user1)
    }

    @Test
    void testPatientIDQueryUser2Xml() {
        performPatientIDQueryTest(XML, user2)
    }

    @Test
    void testPatientIDQueryUser3Xml() {
        performPatientIDQueryTest(XML, user3)
    }

    @Test
    void testPatientIDAsteriskQueryUser1Json() {
        performPatientIDAsteriskQueryTest(JSON, user1, [CT2, CT3, CT5, CT6, CT8, MR4])
    }

    @Test
    void testPatientIDAsteriskQueryUser2Json() {
        performPatientIDAsteriskQueryTest(JSON, user2, [CT2, CT3, CT5, CT6])
    }

    @Test
    void testPatientIDAsteriskQueryUser3Json() {
        performPatientIDAsteriskQueryTest(JSON, user3, [CT2, CT3, CT8, MR4])
    }

    @Test
    void testPatientIDAsteriskQueryUser1Xml() {
        performPatientIDAsteriskQueryTest(XML, user1, [CT2, CT3, CT5, CT6, CT8, MR4])
    }

    @Test
    void testPatientIDAsteriskQueryUser2Xml() {
        performPatientIDAsteriskQueryTest(XML, user2, [CT2, CT3, CT5, CT6])
    }

    @Test
    void testPatientIDAsteriskQueryUser3Xml() {
        performPatientIDAsteriskQueryTest(XML, user3, [CT2, CT3, CT8, MR4])
    }

    @Test
    void testPatientIDQuestionMarkQueryUser1Json() {
        performPatientIDQuestionMarkQueryTest(JSON, user1)
    }

    @Test
    void testPatientIDQuestionMarkQueryUser2Json() {
        performPatientIDQuestionMarkQueryTest(JSON, user2)
    }

    @Test
    void testPatientIDQuestionMarkQueryUser3Json() {
        performPatientIDQuestionMarkQueryTest(JSON, user3)
    }

    @Test
    void testPatientIDQuestionMarkQueryUser1Xml() {
        performPatientIDQuestionMarkQueryTest(XML, user1)
    }

    @Test
    void testPatientIDQuestionMarkQueryUser2Xml() {
        performPatientIDQuestionMarkQueryTest(XML, user2)
    }

    @Test
    void testPatientIDQuestionMarkQueryUser3Xml() {
        performPatientIDQuestionMarkQueryTest(XML, user3)
    }

    @Test
    void testStudyInstanceUIDQueryUser1Json() {
        performStudyInstanceUIDQueryTest(JSON, user1)
    }

    @Test
    void testStudyInstanceUIDQueryUser2Json() {
        performStudyInstanceUIDQueryTest(JSON, user2)
    }

    @Test
    void testStudyInstanceUIDQueryUser3Json() {
        performStudyInstanceUIDQueryTest(JSON, user3)
    }
    
    @Test
    void testStudyInstanceUIDQueryUser1Xml() {
        performStudyInstanceUIDQueryTest(XML, user1)
    }

    @Test
    void testStudyInstanceUIDQueryUser2Xml() {
        performStudyInstanceUIDQueryTest(XML, user2)
    }

    @Test
    void testStudyInstanceUIDQueryUser3Xml() {
        performStudyInstanceUIDQueryTest(XML, user3)
    }
    
    @Test
    void testStudyInstanceUIDListQueryUser1Json() {
        performStudyInstanceUIDListQueryTest(JSON, user1, [CT1, MR3])
    }

    @Test
    void testStudyInstanceUIDListQueryUser2Json() {
        performStudyInstanceUIDListQueryTest(JSON, user2, [CT1])
    }

    @Test
    void testStudyInstanceUIDListQueryUser3Json() {
        performStudyInstanceUIDListQueryTest(JSON, user3, [CT1, MR3])
    }

    @Test
    void testStudyInstanceUIDListQueryUser1Xml() {
        performStudyInstanceUIDListQueryTest(XML, user1, [CT1, MR3])
    }

    @Test
    void testStudyInstanceUIDListQueryUser2Xml() {
        performStudyInstanceUIDListQueryTest(XML, user2, [CT1])
    }

    @Test
    void testStudyInstanceUIDListQueryUser3Xml() {
        performStudyInstanceUIDListQueryTest(XML, user3, [CT1, MR3])
    }

    @Test
    void testStudyIDUniversalMatchingEmptyFieldQueryUser1Json() {
        performStudyIDUniversalMatchingQueryTest(JSON, user1, [CT1, CT2, CT3, CT4, CT5, CT6, CT7, CT8, MR1, MR2, MR3, MR4])
    }

    @Test
    void testStudyIDUniversalMatchingEmptyFieldQueryUser2Json() {
        performStudyIDUniversalMatchingQueryTest(JSON, user2, [CT1, CT2, CT3, CT4, CT5, CT6, MR1, MR2])
    }

    @Test
    void testStudyIDUniversalMatchingEmptyFieldQueryUser3Json() {
        performStudyIDUniversalMatchingQueryTest(JSON, user3, [CT1, CT2, CT3, CT7, CT8, MR1, MR3, MR4])
    }

    @Test
    void testStudyIDUniversalMatchingEmptyFieldQueryUser1Xml() {
        performStudyIDUniversalMatchingQueryTest(XML, user1, [CT1, CT2, CT3, CT4, CT5, CT6, CT7, CT8, MR1, MR2, MR3, MR4])
    }

    @Test
    void testStudyIDUniversalMatchingEmptyFieldQueryUser2Xml() {
        performStudyIDUniversalMatchingQueryTest(XML, user2, [CT1, CT2, CT3, CT4, CT5, CT6, MR1, MR2])
    }

    @Test
    void testStudyIDUniversalMatchingEmptyFieldQueryUser3Xml() {
        performStudyIDUniversalMatchingQueryTest(XML, user3, [CT1, CT2, CT3, CT7, CT8, MR1, MR3, MR4])
    }

    /**
     * Series-level QIDO-RS tests
     */

    @Test
    void testScopedQuerylessSeriesUser1Json() {
        performScopedQuerylessSeriesTest(JSON, user1)
    }

    @Test
    void testScopedQuerylessSeriesUser2Json() {
        performScopedQuerylessSeriesTest(JSON, user2)
    }

    @Test
    void testScopedQuerylessSeriesUser3Json() {
        performScopedQuerylessSeriesTest(JSON, user3)
    }

    @Test
    void testScopedQuerylessSeriesUser1Xml() {
        performScopedQuerylessSeriesTest(XML, user1)
    }

    @Test
    void testScopedQuerylessSeriesUser2Xml() {
        performScopedQuerylessSeriesTest(XML, user2)
    }

    @Test
    void testScopedQuerylessSeriesUser3Xml() {
        performScopedQuerylessSeriesTest(XML, user3)
    }

    @Test
    void testModalityQueryWithSecondaryCaptureSOPClassUser1Json() {
        performModalityQueryWithSecondaryCaptureSOPClassTest(JSON, user1)
    }

    @Test
    void testModalityQueryWithSecondaryCaptureSOPClassUser2Json() {
        performModalityQueryWithSecondaryCaptureSOPClassTest(JSON, user2)
    }

    @Test
    void testModalityQueryWithSecondaryCaptureSOPClassUser3Json() {
        performModalityQueryWithSecondaryCaptureSOPClassTest(JSON, user3)
    }

    @Test
    void testModalityQueryWithSecondaryCaptureSOPClassUser1Xml() {
        performModalityQueryWithSecondaryCaptureSOPClassTest(XML, user1)
    }

    @Test
    void testModalityQueryWithSecondaryCaptureSOPClassUser2Xml() {
        performModalityQueryWithSecondaryCaptureSOPClassTest(XML, user2)
    }

    @Test
    void testModalityQueryWithSecondaryCaptureSOPClassUser3Xml() {
        performModalityQueryWithSecondaryCaptureSOPClassTest(XML, user3)
    }

    @Test
    void testPerformedProcedureStepStartDateMultipleStudyUser1Json() {
        performPerformedProcedureStepStartDateMultipleStudyQueryTest(JSON, user1, CT2.series + CT4.series + CT7.series + MR1.series)
    }

    @Test
    void testPerformedProcedureStepStartDateMultipleStudyUser2Json() {
        performPerformedProcedureStepStartDateMultipleStudyQueryTest(JSON, user2, CT2.series + CT4.series + MR1.series)
    }

    @Test
    void testPerformedProcedureStepStartDateMultipleStudyUser3Json() {
        performPerformedProcedureStepStartDateMultipleStudyQueryTest(JSON, user3, CT2.series + CT7.series + MR1.series)
    }

    @Test
    void testPerformedProcedureStepStartDateMultipleStudyUser1Xml() {
        performPerformedProcedureStepStartDateMultipleStudyQueryTest(XML, user1, CT2.series + CT4.series + CT7.series + MR1.series)
    }

    @Test
    void testPerformedProcedureStepStartDateMultipleStudyUser2Xml() {
        performPerformedProcedureStepStartDateMultipleStudyQueryTest(XML, user2, CT2.series + CT4.series + MR1.series)
    }

    @Test
    void testPerformedProcedureStepStartDateMultipleStudyUser3Xml() {
        performPerformedProcedureStepStartDateMultipleStudyQueryTest(XML, user3, CT2.series + CT7.series + MR1.series)
    }

    private void performStudyDateQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyDate, CT1.studyDate).expectedResult(expectedResult))
    }

    private void performStudyDateRangeQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyDate, '20170303-20170505').expectedResult(expectedResult))
    }

    private void performStudyDateRangeOpenEndpointQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> firstExpectedResult, Collection<DicomWebTestStudy> secondExpectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyDate, '-20170404').expectedResult(firstExpectedResult))
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyDate, '20170303-').expectedResult(secondExpectedResult))
    }

    private void performStudyTimeQueryTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyTime, CT1.studyTime).expectedResult(CT1))
    }

    private void performStudyTimeRangeQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        ['090909.123456-19', '090909-195900'].each { studyTimeRange ->
            execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyTime, studyTimeRange).expectedResult(expectedResult))
        }
    }

    private void performStudyTimeOpenEndpointQueryTest(QidoRequestContentType contentType, User user, boolean openAtFront, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyTime, openAtFront ? '-14' : '14-').expectedResult(expectedResult))
    }

    private void performStudyDateStudyTimeCombinedMatchingQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).querySearchParams([(StudyDate) : '20170303-20170404', (StudyTime) : '1530-11']).expectedResult(expectedResult))
    }

    private void performAccessionNumberQueryTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(AccessionNumber, CT1.accessionNumber).expectedResult(CT1))
    }

    private void performAccessionNumberAsteriskQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(AccessionNumber, '2*').expectedResult(expectedResult))
    }

    private void performAccessionNumberQuestionMarkQueryTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(AccessionNumber, CT2.accessionNumber.replace('1', '?')).expectedResult(CT2))
    }

    private void performModalitiesInStudyQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(ModalitiesInStudy, 'CT').expectedResult(expectedResult))
    }

    private void performPatientNameQueryTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(PatientName, CT1.patientName).expectedResult([CT1, MR1]))
    }

    private void performPatientNameAsteriskQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        ['TCGA-14*', 'tcga-14*'].each { patientName ->
            execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(PatientName, patientName).expectedResult(expectedResult))
        }
    }

    private void performPatientNameQuestionMarkQueryTest(QidoRequestContentType contentType, User user) {
        ['TCGA-CS-618?', 'tcga-CS-618?'].each { patientName ->
            execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(PatientName, patientName).expectedResult([CT1, MR1]))
        }
    }

    private void performPatientNameCaseInsensitiveQueryTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(PatientName, CT1.patientName.toLowerCase()).expectedResult([CT1, MR1]))
    }

    private void performPatientIDQueryTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(PatientID, CT1.patientID).expectedResult([CT1, MR1]))
    }

    private void performPatientIDAsteriskQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(PatientID, 'TCGA-14*').expectedResult(expectedResult))
    }

    private void performPatientIDQuestionMarkQueryTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(PatientID, 'TCGA-?S-618?').expectedResult([CT1, MR1]))
    }

    private void performStudyInstanceUIDQueryTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyInstanceUID, CT1.studyInstanceUID).expectedResult(CT1))
    }

    private void performStudyInstanceUIDListQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyInstanceUID, [CT1.studyInstanceUID, MR3.studyInstanceUID, '1.2.3.4.5.6.7.8']).expectedResult(expectedResult))
    }

    private void performStudyIDUniversalMatchingQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) { // PS3.4 C.2.2.2.3 - should match everything
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(StudyID, '*').expectedResult(expectedResult))
    }

    private void performScopedQuerylessSeriesTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(CT4).contentType(contentType).user(user).expectedResult(user == user3 ? [] : CT4.series))
    }

    private void performModalityQueryWithSecondaryCaptureSOPClassTest(QidoRequestContentType contentType, User user) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(CT4).contentType(contentType).user(user).simpleQuery(Modality, 'CT').expectedResult(user == user3 ? [] : CT4.series))
    }

    private void performPerformedProcedureStepStartDateMultipleStudyQueryTest(QidoRequestContentType contentType, User user, Collection<DicomWebTestSeries> expectedResult) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(user).simpleQuery(PerformedProcedureStepStartDate, '20170101').expectedResult(expectedResult))
    }

}
