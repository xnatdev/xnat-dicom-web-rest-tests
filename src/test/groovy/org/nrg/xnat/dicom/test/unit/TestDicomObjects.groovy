package org.nrg.xnat.dicom.test.unit

import org.nrg.testing.DicomUtils
import org.nrg.xnat.dicom.model.DicomAttribute
import org.nrg.xnat.dicom.model.DicomObject
import org.testng.annotations.Test

import static org.testng.AssertJUnit.assertEquals

class TestDicomObjects {

    @Test
    void testMissingStudyAttributes() {
        final DicomObject dicomObject = new DicomObject()
        DicomObject.REQUIRED_STUDY_ATTRIBUTES.each { integer ->
            dicomObject.getAttributes().add(new DicomAttribute<>(String.class).tag(DicomUtils.intToSimpleHeaderString(integer)))
        }
        assertEquals([], dicomObject.missingStudyAttributes())

        final DicomAttribute firstRemoved = dicomObject.getAttributes().remove(0)
        assertEquals([firstRemoved.getTag()], dicomObject.missingStudyAttributes())

        final DicomAttribute secondRemoved = dicomObject.getAttributes().remove(4)
        assertEquals([firstRemoved.getTag(), secondRemoved.getTag()], dicomObject.missingStudyAttributes())
    }

}
