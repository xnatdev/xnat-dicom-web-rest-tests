package org.nrg.xnat.dicom.test.integration

import org.nrg.testing.annotations.TestRequires
import org.nrg.testing.enums.TestData
import org.nrg.xnat.dicom.wado.WadoTestSpec
import org.nrg.xnat.dicom.web.BaseDicomWebTest
import org.nrg.xnat.dicom.web.DicomWebTestInstance
import org.nrg.xnat.dicom.web.DicomWebTestSeries
import org.nrg.xnat.dicom.web.DicomWebTestStudy
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.ImagingSession
import org.nrg.xnat.pogo.experiments.sessions.CTSession
import org.nrg.xnat.pogo.experiments.sessions.MRSession
import org.nrg.xnat.pogo.experiments.sessions.PETSession
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension
import org.testng.annotations.Test

import static org.nrg.xnat.dicom.web.DicomWebTestStudy.*
import static org.nrg.xnat.dicom.web.DicomWebTestSeries.*
import static org.nrg.xnat.dicom.web.DicomWebTestInstance.*
import static org.nrg.xnat.dicom.enums.WadoRequestType.*
import static org.nrg.xnat.dicom.enums.WadoRequestContentType.*
import static org.nrg.xnat.dicom.enums.WadoValidationType.*
import static org.dcm4che3.data.UID.*

@TestRequires(
        data = [
                TestData.DICOM_WEB_PETMR2_MR,
                TestData.DICOM_WEB_PETMR2_PT,
                TestData.DICOM_WEB_MULTIFRAME_CT,
                TestData.BIG_ENDIAN,
                TestData.JPEGLOSSLESS_2000,
                TestData.DICOM_WEB_CT1,
                TestData.MIXED_FRAME_STUDY
        ]
)
class TestsWithDifferentTransferSyntaxes extends BaseDicomWebTest {

    private static final List<String> UNSUPPORTED_TRANSFER_SYNTAXES = [JPEGFullProgressionHierarchical2527Retired, JPIPReferenced, HEVCH265MainProfileLevel51, ExplicitVRBigEndianRetired] // not supported for *any* type of image
    private static final List<String> SUPPORTED_SINGLE_FRAME_JPEGLOSSLESS_SYNTAXES = [JPEGLossless, JPEGLosslessNonHierarchical14, JPEGLSLossless, JPEG2000LosslessOnly]
    private static final String DEFAULT_CHARSET = 'UTF-8'

    private final List<DicomWebTestStudy> allStudies = [PETMR2, MULTIFRAME_CT, BIG_ENDIAN_STUDY, JPEGLOSSLESS, CT1]
    private final List<DicomWebTestSeries> allSeries = allStudies.series.flatten() as List<DicomWebTestSeries>
    private final List<DicomWebTestInstance> allInstances = allSeries.knownInstances.flatten() as List<DicomWebTestInstance>
    private final List<DicomWebTestStudy> defaultTSStudies = [MULTIFRAME_CT, CT1]
    private final List<DicomWebTestSeries> defaultTSSeries = (defaultTSStudies.series.flatten() as List<DicomWebTestSeries>) + [PETMR2_MR_5610035, PETMR2_MR_5616046] // MR series in PETMR2 encoded as LE-Explicit
    private final List<DicomWebTestInstance> defaultTSInstances = defaultTSSeries.knownInstances.flatten() as List<DicomWebTestInstance>
    private final List<DicomWebTestStudy> singleFrameStudies = [PETMR2, BIG_ENDIAN_STUDY, JPEGLOSSLESS, CT1] // *whole* study is composed of single frame images
    private final List<DicomWebTestSeries> singleFrameSeries = (singleFrameStudies.series.flatten() as List<DicomWebTestSeries>) + MIXED_FRAME_MR_100 // each series is *wholly* composed of single frame images
    private final List<DicomWebTestInstance> singleFrameInstances = singleFrameSeries.knownInstances.flatten() as List<DicomWebTestInstance>

    @SuppressWarnings("GroovyResultOfObjectAllocationIgnored")
    @Override
    List<Project> defineClassProjects() {
        final Project project = new Project().addMember(mainUser)

        final Subject implicitVRSubject = new Subject(project)
        final Subject multiframeCTSubject = new Subject(project)
        final Subject bigEndianSubject = new Subject(project)
        final Subject jpeglosslessSubject = new Subject(project)
        final Subject standardCTSubject = new Subject(project)
        final Subject mixedFrameSubject = new Subject(project)

        final PETSession implicitVRPetHalf = new PETSession(project, implicitVRSubject)
        final MRSession implicitVRMRHalf = new MRSession(project, implicitVRSubject)
        final CTSession multiframeCT = new CTSession(project, multiframeCTSubject)
        final ImagingSession bigEndianStudy = new ImagingSession(project, bigEndianSubject)
        final MRSession jpeglosslessStudy = new MRSession(project, jpeglosslessSubject)
        final CTSession standardCT = new CTSession(project, standardCTSubject)
        final MRSession mixedFrameStudy = new MRSession(project, mixedFrameSubject)

        addImportExtension(implicitVRPetHalf, PETMR2)
        new SessionImportExtension(implicitVRMRHalf, TestData.DICOM_WEB_PETMR2_MR.toFile())
        addImportExtension(multiframeCT, MULTIFRAME_CT)
        addImportExtension(bigEndianStudy, BIG_ENDIAN_STUDY)
        addImportExtension(jpeglosslessStudy, JPEGLOSSLESS)
        addImportExtension(standardCT, CT1)
        addImportExtension(mixedFrameStudy, MIXED_FRAME)

        [project]
    }

    // TODO: to and from lossy encodings

    @Test
    void testUnsupportedTransferSyntaxStudyRetrieve() {
        allStudies.each { study ->
            UNSUPPORTED_TRANSFER_SYNTAXES.each { tsUid ->
                execute(new WadoTestSpec(STUDY).studyInstanceUID(study).contentType(DICOM).transferSyntax(tsUid).user(mainUser).expectedStatusCode(406).expectedBody("Unsupported transfer syntax: ${tsUid}"))
            }
        }
    }

    @Test
    void testUnsupportedTransferSyntaxSeriesRetrieve() {
        allSeries.each { series ->
            UNSUPPORTED_TRANSFER_SYNTAXES.each { tsUid ->
                execute(new WadoTestSpec(SERIES).seriesInstanceUID(series).contentType(DICOM).transferSyntax(tsUid).user(mainUser).expectedStatusCode(406).expectedBody("Unsupported transfer syntax: ${tsUid}"))
            }
        }
    }

    @Test
    void testUnsupportedTransferSyntaxInstanceRetrieve() {
        allInstances.each { instance ->
            UNSUPPORTED_TRANSFER_SYNTAXES.each { tsUid ->
                execute(new WadoTestSpec(INSTANCE).sopInstanceUID(instance).contentType(DICOM).transferSyntax(tsUid).user(mainUser).expectedStatusCode(406))
            }
        }
    }

    @Test
    void testMultipleUnsupportedTransferSyntaxStudyRetrieve() {
        allStudies.each { study ->
            execute(new WadoTestSpec(STUDY).studyInstanceUID(study).contentType(DICOM).transferSyntaxes(UNSUPPORTED_TRANSFER_SYNTAXES).user(mainUser).expectedStatusCode(406))
        }
    }

    @Test
    void testMultipleUnsupportedTransferSyntaxSeriesRetrieve() {
        allSeries.each { series ->
            execute(new WadoTestSpec(SERIES).seriesInstanceUID(series).contentType(DICOM).transferSyntaxes(UNSUPPORTED_TRANSFER_SYNTAXES).user(mainUser).expectedStatusCode(406))
        }
    }

    @Test
    void testMultipleUnsupportedTransferSyntaxInstanceRetrieve() {
        allInstances.each { instance ->
            execute(new WadoTestSpec(INSTANCE).sopInstanceUID(instance).contentType(DICOM).transferSyntaxes(UNSUPPORTED_TRANSFER_SYNTAXES).user(mainUser).expectedStatusCode(406))
        }
    }

    @Test
    void testMultipleUnsupportedAndSupportedTransferSyntaxStudyRetrieve() {
        singleFrameStudies.each { study ->
            execute(new WadoTestSpec(STUDY).studyInstanceUID(study).contentType(DICOM).transferSyntaxes(UNSUPPORTED_TRANSFER_SYNTAXES + JPEGLossless).user(mainUser).expectedResult(study).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
        }
    }

    @Test
    void testMultipleUnsupportedAndSupportedTransferSyntaxSeriesRetrieve() {
        singleFrameSeries.each { series ->
            execute(new WadoTestSpec(SERIES).seriesInstanceUID(series).contentType(DICOM).transferSyntaxes(UNSUPPORTED_TRANSFER_SYNTAXES + JPEGLossless).user(mainUser).expectedResult(series).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
        }
    }

    @Test
    void testMultipleUnsupportedAndSupportedTransferSyntaxInstanceRetrieve() {
        singleFrameInstances.each { instance ->
            execute(new WadoTestSpec(INSTANCE).sopInstanceUID(instance).contentType(DICOM).transferSyntaxes(UNSUPPORTED_TRANSFER_SYNTAXES + JPEGLossless).user(mainUser).expectedResult(instance).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
        }
    }

    @Test
    void testSingleFrameTransferSyntaxForMultiframeStudy() {
        execute(new WadoTestSpec(STUDY).studyInstanceUID(MULTIFRAME_CT).contentType(DICOM).transferSyntax(JPEGLossless).user(mainUser).expectedStatusCode(406)) // JPEGLossless transfer syntax only supported for single frame images
    }

    @Test
    void testSingleFrameTransferSyntaxForMultiframeSeries() {
        MULTIFRAME_CT.series.each { series ->
            execute(new WadoTestSpec(SERIES).seriesInstanceUID(series).contentType(DICOM).transferSyntax(JPEGLossless).user(mainUser).expectedStatusCode(406)) // JPEGLossless transfer syntax only supported for single frame images
        }
    }

    @Test
    void testSingleFrameTransferSyntaxForMultiframeInstance() {
        (MULTIFRAME_CT.series.knownInstances.flatten() as Collection<DicomWebTestInstance>).each { instance ->
            execute(new WadoTestSpec(INSTANCE).sopInstanceUID(instance).contentType(DICOM).transferSyntax(JPEGLossless).user(mainUser).expectedStatusCode(406)) // JPEGLossless transfer syntax only supported for single frame images
        }
    }

    @Test
    void testWildcardTransferSyntaxWithoutTranscodingStudyRetrieve() {
        defaultTSStudies.each { study ->
            execute(new WadoTestSpec(STUDY).studyInstanceUID(study).contentType(DICOM).transferSyntax('*').user(mainUser).expectedResult(study))
        }
    }

    @Test
    void testWildcardTransferSyntaxWithoutTranscodingSeriesRetrieve() {
        defaultTSSeries.each { series ->
            execute(new WadoTestSpec(SERIES).seriesInstanceUID(series).contentType(DICOM).transferSyntax('*').user(mainUser).expectedResult(series))
        }
    }

    @Test
    void testWildcardTransferSyntaxWithoutTranscodingInstanceRetrieve() {
        defaultTSInstances.each { instance ->
            execute(new WadoTestSpec(INSTANCE).sopInstanceUID(instance).contentType(DICOM).transferSyntax('*').user(mainUser).expectedResult(instance))
        }
    }

    @Test
    void testWildcardTransferSyntaxWithTranscodingStudyRetrieve() {
        (allStudies - defaultTSStudies).each { study ->
            execute(new WadoTestSpec(STUDY).studyInstanceUID(study).contentType(DICOM).transferSyntax('*').user(mainUser).expectedResult(study).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
        }
    }

    @Test
    void testWildcardTransferSyntaxWithTranscodingSeriesRetrieve() {
        (allSeries - defaultTSSeries).each { series ->
            execute(new WadoTestSpec(SERIES).seriesInstanceUID(series).contentType(DICOM).transferSyntax('*').user(mainUser).expectedResult(series).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
        }
    }

    @Test
    void testWildcardTransferSyntaxWithTranscodingInstanceRetrieve() {
        (allInstances - defaultTSInstances).each { instance ->
            execute(new WadoTestSpec(INSTANCE).sopInstanceUID(instance).contentType(DICOM).transferSyntax('*').user(mainUser).expectedResult(instance).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
        }
    }

    @Test
    void testSpecifiedCharsetStudyRetrieve() {
        execute(new WadoTestSpec(STUDY).studyInstanceUID(CT1).contentType(DICOM).charset(DEFAULT_CHARSET).user(mainUser).expectedResult(CT1))
    }

    @Test
    void testSpecifiedCharsetSeriesRetrieve() {
        execute(new WadoTestSpec(SERIES).seriesInstanceUID(CT1_CT_3).contentType(DICOM).charset(DEFAULT_CHARSET).user(mainUser).expectedResult(CT1_CT_3))
    }

    @Test
    void testSpecifiedCharsetInstanceRetrieve() {
        execute(new WadoTestSpec(INSTANCE).sopInstanceUID(CT1_CT_3_INSTANCE).contentType(DICOM).charset(DEFAULT_CHARSET).user(mainUser).expectedResult(CT1_CT_3_INSTANCE))
    }

    @Test
    void testSingleFrameJPEGLosslessStudyRetrieve() {
        SUPPORTED_SINGLE_FRAME_JPEGLOSSLESS_SYNTAXES.each { syntax ->
            singleFrameStudies.each { study ->
                execute(new WadoTestSpec(STUDY).studyInstanceUID(study).contentType(DICOM).transferSyntax(syntax).user(mainUser).expectedResult(study).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
            }
        }
    }

    @Test
    void testSingleFrameJPEGLosslessSeriesRetrieve() {
        SUPPORTED_SINGLE_FRAME_JPEGLOSSLESS_SYNTAXES.each { syntax ->
            singleFrameSeries.each { series ->
                execute(new WadoTestSpec(SERIES).seriesInstanceUID(series).contentType(DICOM).transferSyntax(syntax).user(mainUser).expectedResult(series).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
            }
        }
    }

    @Test
    void testSingleFrameJPEGLosslessInstanceRetrieve() {
        SUPPORTED_SINGLE_FRAME_JPEGLOSSLESS_SYNTAXES.each { syntax ->
            singleFrameInstances.each { instance ->
                execute(new WadoTestSpec(INSTANCE).sopInstanceUID(instance).contentType(DICOM).transferSyntax(syntax).user(mainUser).expectedResult(instance).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
            }
        }
    }

    @Test
    void testMultiframeJPEGLosslessStudyRetrieve() {
        execute(new WadoTestSpec(STUDY).studyInstanceUID(MULTIFRAME_CT).contentType(DICOM).transferSyntax(JPEG2000LosslessOnly).user(mainUser).expectedResult(MULTIFRAME_CT).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
    }

    @Test
    void testMultiframeJPEGLosslessSeriesRetrieve() {
        (allSeries - singleFrameSeries).each { series ->
            execute(new WadoTestSpec(SERIES).seriesInstanceUID(series).contentType(DICOM).transferSyntax(JPEG2000LosslessOnly).user(mainUser).expectedResult(series).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
        }
    }

    @Test
    void testMultiframeJPEGLosslessInstanceRetrieve() {
        (allInstances - singleFrameInstances).each { instance ->
            execute(new WadoTestSpec(INSTANCE).sopInstanceUID(instance).contentType(DICOM).transferSyntax(JPEG2000LosslessOnly).user(mainUser).expectedResult(instance).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING))
        }
    }

    @Test
    void testPartialTransferSyntaxSupportStudyRetrieve() {
        execute(new WadoTestSpec(STUDY).studyInstanceUID(MIXED_FRAME).contentType(DICOM).transferSyntax(JPEGLossless).user(mainUser).expectedResult(MIXED_FRAME_MR_100).expectedStatusCode(206).validationType(IDENTICAL_IMAGES_AFTER_TRANSCODING)) // only the single frame instances can be transcoded this way
    }

}
