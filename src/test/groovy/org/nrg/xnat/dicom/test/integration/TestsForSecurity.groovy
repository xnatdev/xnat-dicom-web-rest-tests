package org.nrg.xnat.dicom.test.integration

import org.nrg.testing.annotations.TestRequires
import org.nrg.testing.enums.TestData
import org.nrg.xnat.dicom.enums.WadoRequestContentType
import org.nrg.xnat.dicom.enums.WadoRequestType
import org.nrg.xnat.dicom.wado.WadoTestSpec
import org.nrg.xnat.dicom.web.BaseDicomWebTest
import org.nrg.xnat.dicom.web.DicomWebTestInstance
import org.nrg.xnat.dicom.web.DicomWebTestSeries
import org.nrg.xnat.dicom.web.DicomWebTestStudy
import org.nrg.xnat.dicom.qido.QidoTestSpec
import org.nrg.xnat.dicom.enums.QidoRequestContentType
import org.nrg.xnat.enums.Accessibility
import org.nrg.xnat.enums.Gender
import org.nrg.xnat.pogo.Project
import org.nrg.xnat.pogo.Share
import org.nrg.xnat.pogo.Subject
import org.nrg.xnat.pogo.experiments.sessions.*
import org.nrg.xnat.pogo.extensions.subject_assessor.SessionImportExtension
import org.nrg.xnat.pogo.users.CustomUserGroup
import org.nrg.xnat.pogo.users.User
import org.testng.annotations.Test

import static org.dcm4che3.data.Tag.*
import static org.nrg.xnat.dicom.web.DicomWebTestStudy.*
import static org.nrg.xnat.dicom.web.DicomWebTestSeries.*
import static org.nrg.xnat.dicom.enums.QidoRequestContentType.*
import static org.nrg.xnat.dicom.enums.QidoRequestType.*
import static org.nrg.xnat.dicom.enums.WadoRequestContentType.*
import static org.nrg.xnat.enums.DataAccessLevel.*
import static org.nrg.xnat.pogo.DataType.*

@TestRequires(
        users = 2,
        data = [
                TestData.DICOM_WEB_CT1,
                TestData.DICOM_WEB_CT2,
                TestData.DICOM_WEB_CT3,
                TestData.DICOM_WEB_PETCT2_CT,
                TestData.DICOM_WEB_PETCT2_PET,
                TestData.DICOM_WEB_PETMR2_MR,
                TestData.DICOM_WEB_PETMR2_PT,
                TestData.DICOM_WEB_PETMR4_MR,
                TestData.DICOM_WEB_PETMR4_PT
        ]
)
class TestsForSecurity extends BaseDicomWebTest {

    private final DicomWebTestStudy PETMR2_PT = PETMR2.clone().series([PETMR2_PT_4])
    // private final DicomWebTestStudy PETMR2_MR = PETMR2.clone().series([PETMR2_MR_5610035, PETMR2_MR_5616046])
    private final DicomWebTestStudy PETCT2_CT = PETCT2.clone().series([PETCT2_CT_2])
    // private final DicomWebTestStudy PETCT2_PT = PETCT2.clone().series([PETCT2_PT_605, PETCT2_PT_606])
    private final DicomWebTestStudy PETMR4_PT = PETMR4.clone().series([PETMR4_PT_1]) // readable by customUserGroupUser
    // private final DicomWebTestStudy PETMR4_MR = PETMR4.clone().series([PETMR4_MR_4488021, PETMR4_MR_4494017])

    private final List<DicomWebTestStudy> publicStudies = [CT1, CT3, PETMR2_PT, PETCT2_CT]
    private final List<DicomWebTestSeries> publicSeries = publicStudies.series.flatten() as List<DicomWebTestSeries>
    private final List<DicomWebTestInstance> publicInstances = publicSeries.knownInstances.flatten() as List<DicomWebTestInstance>
    private final List<DicomWebTestStudy> allStudies = [PETMR2, PETCT2, PETMR4, CT1, CT2, CT3]
    private final List<DicomWebTestSeries> allSeries = allStudies.series.flatten() as List<DicomWebTestSeries>
    private final List<DicomWebTestInstance> allInstances = allSeries.knownInstances.flatten() as List<DicomWebTestInstance>
    private final List<DicomWebTestStudy> studiesReadableByCustomGroupUser = publicStudies + PETMR4_PT
    private final List<DicomWebTestSeries> seriesReadableByCustomGroupUser = studiesReadableByCustomGroupUser.series.flatten() as List<DicomWebTestSeries>
    private final List<DicomWebTestInstance> instancesReadableByCustomGroupUser = seriesReadableByCustomGroupUser.knownInstances.flatten() as List<DicomWebTestInstance>

    private final Project publicProject = new Project().accessibility(Accessibility.PUBLIC)
    private final Project privateProject = new Project().accessibility(Accessibility.PRIVATE)
    private final Project protectedProject = new Project().accessibility(Accessibility.PROTECTED)
    private final Project customUserGroupProject = new Project().accessibility(Accessibility.PRIVATE)
    private Subject ct1Subject, splitPetMrPublicSubject, splitPetMrPrivateSubject, multipleCtSubject, petCtSubject, customUserGroupSubject
    private CTSession ct1, ct2, ct3, petCtPublicHalf
    private PETSession splitPetMrPublicHalf, petCtPrivateHalf, customUserGroupPt
    private MRSession splitPetMrPrivateHalf, customUserGroupMr
    private User customUserGroupUser, secondaryAdmin

    @SuppressWarnings("GroovyResultOfObjectAllocationIgnored")
    @Override
    List<Project> defineClassProjects() {
        customUserGroupUser = getGenericUser()
        secondaryAdmin = getGenericUser()
        mainAdminInterface().makeUserAdmin(secondaryAdmin)

        final CustomUserGroup petTechGroup = new CustomUserGroup('PET Tech')
        petTechGroup.accessLevelMap[SUBJECT] = READ_ONLY
        petTechGroup.accessLevelMap[PET_SESSION] = CREATE_AND_EDIT
        customUserGroupProject.addUserGroup(petTechGroup, [customUserGroupUser])

        ct1Subject = new Subject(publicProject).gender(Gender.MALE)
        splitPetMrPublicSubject = new Subject(publicProject).dob(PETMR2.birthDateObject())
        splitPetMrPrivateSubject = new Subject(privateProject).dob(PETMR2.birthDateObject())
        multipleCtSubject = new Subject(protectedProject).gender(Gender.MALE).addShare(new Share(publicProject))
        petCtSubject = new Subject(privateProject).dob(PETCT2.birthDateObject()).addShare(new Share(publicProject))
        customUserGroupSubject = new Subject(customUserGroupProject).gender(Gender.MALE)

        ct1 = new CTSession(publicProject, ct1Subject)
        splitPetMrPublicHalf = new PETSession(publicProject, splitPetMrPublicSubject)
        splitPetMrPrivateHalf = new MRSession(privateProject, splitPetMrPrivateSubject)
        ct2 = new CTSession(protectedProject, multipleCtSubject)
        ct3 = new CTSession(protectedProject, multipleCtSubject).addShare(new Share(publicProject))
        petCtPublicHalf = new CTSession(privateProject, petCtSubject).addShare(new Share(publicProject))
        petCtPrivateHalf = new PETSession(privateProject, petCtSubject)
        customUserGroupPt = new PETSession(customUserGroupProject, customUserGroupSubject)
        customUserGroupMr = new MRSession(customUserGroupProject, customUserGroupSubject)

        addImportExtension(ct1, CT1)
        addImportExtension(splitPetMrPublicHalf, PETMR2)
        addImportExtension(ct2, CT2)
        addImportExtension(ct3, CT3)
        addImportExtension(petCtPrivateHalf, PETCT2)
        addImportExtension(customUserGroupPt, PETMR4)
        new SessionImportExtension(petCtPublicHalf, TestData.DICOM_WEB_PETCT2_CT.toFile())
        new SessionImportExtension(splitPetMrPrivateHalf, TestData.DICOM_WEB_PETMR2_MR.toFile())
        new SessionImportExtension(customUserGroupMr, TestData.DICOM_WEB_PETMR4_MR.toFile())

        [publicProject, privateProject, protectedProject, customUserGroupProject]
    }

    /**
     * QIDO-RS tests
     */

    @Test
    @TestRequires(openXnat = true)
    void testGuestStudiesOpenXnatJsonQuery() {
        performGuestStudiesQuery(JSON, true)
    }

    @Test
    @TestRequires(openXnat = true)
    void testGuestStudiesOpenXnatXmlQuery() {
        performGuestStudiesQuery(XML, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestStudiesClosedXnatJsonQuery() {
        performGuestStudiesQuery(JSON, false)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestStudiesClosedXnatXmlQuery() {
        performGuestStudiesQuery(XML, false)
    }

    @Test
    @TestRequires(openXnat = true)
    void testGuestSeriesOpenXnatJsonQuery() {
        performGuestSeriesQuery(JSON, true)
    }

    @Test
    @TestRequires(openXnat = true)
    void testGuestSeriesOpenXnatXmlQuery() {
        performGuestSeriesQuery(XML, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestSeriesClosedXnatJsonQuery() {
        performGuestSeriesQuery(JSON, false)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestSeriesClosedXnatXmlQuery() {
        performGuestSeriesQuery(XML, false)
    }

    @Test
    @TestRequires(openXnat = true)
    void testScopedGuestSeriesOpenXnatJsonQuery() {
        performScopedGuestSeriesQuery(JSON, true)
    }

    @Test
    @TestRequires(openXnat = true)
    void testScopedGuestSeriesOpenXnatXmlQuery() {
        performScopedGuestSeriesQuery(XML, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testScopedGuestSeriesClosedXnatJsonQuery() {
        performScopedGuestSeriesQuery(JSON, false)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testScopedGuestSeriesClosedXnatXmlQuery() {
        performScopedGuestSeriesQuery(XML, false)
    }

    @Test
    void testAdminStudiesJsonQuery() {
        performAuthenticatedStudiesQuery(JSON, secondaryAdmin, allStudies)
    }

    @Test
    void testAdminStudiesXmlQuery() {
        performAuthenticatedStudiesQuery(XML, secondaryAdmin, allStudies)
    }

    @Test
    void testAdminSeriesJsonQuery() {
        performAuthenticatedSeriesQuery(JSON, secondaryAdmin, allSeries)
    }

    @Test
    void testAdminSeriesXmlQuery() {
        performAuthenticatedSeriesQuery(XML, secondaryAdmin, allSeries)
    }

    @Test
    void testScopedAdminSeriesJsonQuery() {
        allStudies.each { study ->
            performScopedAuthenticatedSeriesQuery(JSON, secondaryAdmin, study, study.series)
        }
    }

    @Test
    void testScopedAdminSeriesXmlQuery() {
        allStudies.each { study ->
            performScopedAuthenticatedSeriesQuery(XML, secondaryAdmin, study, study.series)
        }
    }

    @Test
    void testCustomUserGroupStudiesJsonQuery() {
        performAuthenticatedStudiesQuery(JSON, customUserGroupUser, studiesReadableByCustomGroupUser)
    }

    @Test
    void testCustomUserGroupStudiesXmlQuery() {
        performAuthenticatedStudiesQuery(XML, customUserGroupUser, studiesReadableByCustomGroupUser)
    }

    @Test
    void testCustomUserGroupSeriesJsonQuery() {
        performAuthenticatedSeriesQuery(JSON, customUserGroupUser, seriesReadableByCustomGroupUser)
    }

    @Test
    void testCustomUserGroupSeriesXmlQuery() {
        performAuthenticatedSeriesQuery(XML, customUserGroupUser, seriesReadableByCustomGroupUser)
    }

    @Test
    void testScopedCustomUserGroupSeriesJsonQuery() {
        performScopedAuthenticatedSeriesQuery(JSON, customUserGroupUser, PETMR4_PT, PETMR4_PT.series)
    }

    @Test
    void testScopedCustomUserGroupSeriesXmlQuery() {
        performScopedAuthenticatedSeriesQuery(XML, customUserGroupUser, PETMR4_PT, PETMR4_PT.series)
    }

    /**
     * WADO-RS tests
     */

    @Test
    @TestRequires(openXnat = true)
    void testGuestStudyOpenXnatRetrieve() {
        performGuestStudyRetrieveTest(DICOM, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestStudyClosedXnatRetrieve() {
        performGuestStudyRetrieveTest(DICOM, false)
    }

    @Test
    @TestRequires(openXnat = true)
    void testGuestSplitStudyOpenXnatRetrieve() {
        performGuestSplitStudyRetrieveTest(DICOM, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestSplitStudyClosedXnatRetrieve() {
        performGuestSplitStudyRetrieveTest(DICOM, false)
    }

    @Test
    @TestRequires(openXnat = true)
    void testGuestSeriesOpenXnatRetrieve() {
        performGuestSeriesRetrieveTest(DICOM, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestSeriesClosedXnatRetrieve() {
        performGuestSeriesRetrieveTest(DICOM, false)
    }

    @Test
    @TestRequires(openXnat = true)
    void testGuestInaccessibleSeriesOpenXnatRetrieve() {
        performGuestInaccessibleSeriesRetrieveTest(DICOM, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestInaccessibleSeriesClosedXnatRetrieve() {
        performGuestInaccessibleSeriesRetrieveTest(DICOM, false)
    }

    @Test
    @TestRequires(openXnat = true)
    void testGuestInstanceOpenXnatRetrieve() {
        performGuestInstanceRetrieveTest(DICOM, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestInstanceClosedXnatRetrieve() {
        performGuestInstanceRetrieveTest(DICOM, false)
    }
    
    @Test
    @TestRequires(openXnat = true)
    void testGuestInaccessibleInstanceOpenXnatRetrieve() {
        performGuestInaccessibleInstanceRetrieveTest(DICOM, true)
    }

    @Test
    @TestRequires(closedXnat = true)
    void testGuestInaccessibleInstanceClosedXnatRetrieve() {
        performGuestInaccessibleInstanceRetrieveTest(DICOM, false)
    }
    
    @Test
    void testAdminStudyRetrieve() {
        performAuthenticatedStudyRetrieveTest(DICOM, secondaryAdmin, allStudies)
    }

    @Test
    void testAdminSeriesRetrieve() {
        performAuthenticatedSeriesRetrieveTest(DICOM, secondaryAdmin, allSeries)
    }

    @Test
    void testAdminInstanceRetrieve() {
        performAuthenticatedInstanceRetrieveTest(DICOM, secondaryAdmin, allInstances)
    }

    @Test
    void testCustomUserGroupUserStudyRetrieve() {
        performAuthenticatedStudyRetrieveTest(DICOM, customUserGroupUser, studiesReadableByCustomGroupUser)
    }

    @Test
    void testCustomUserGroupUserInaccessibleStudyRetrieve() {
        performAuthenticatedInaccessibleStudyRetrieveTest(DICOM, customUserGroupUser, [CT2]) // CT2 is the only study that the user doesn't have at least partial access to
    }

    @Test
    void testCustomUserGroupUserSeriesRetrieve() {
        performAuthenticatedSeriesRetrieveTest(DICOM, customUserGroupUser, seriesReadableByCustomGroupUser)
    }

    @Test
    void testCustomUserGroupUserInaccessibleSeriesRetrieve() {
        performAuthenticatedInaccessibleSeriesRetrieveTest(DICOM, customUserGroupUser, allSeries - seriesReadableByCustomGroupUser)
    }

    @Test
    void testCustomUserGroupUserInstanceRetrieve() {
        performAuthenticatedInstanceRetrieveTest(DICOM, customUserGroupUser, instancesReadableByCustomGroupUser)
    }

    @Test
    void testCustomUserGroupUserInaccessibleInstanceRetrieve() {
        performAuthenticatedInaccessibleInstanceRetrieveTest(DICOM, customUserGroupUser, allInstances - instancesReadableByCustomGroupUser)
    }

    private void performGuestStudiesQuery(QidoRequestContentType contentType, boolean openXnat) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).simpleQuery(AccessionNumber, '*').expectedResultIfGuestAllowed(openXnat, publicStudies))
    }

    private void performGuestSeriesQuery(QidoRequestContentType contentType, boolean openXnat) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).simpleQuery(Modality, '*').expectedResultIfGuestAllowed(openXnat, publicSeries))
    }

    private void performScopedGuestSeriesQuery(QidoRequestContentType contentType, boolean openXnat) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETMR2.studyInstanceUID).contentType(contentType).expectedResultIfGuestAllowed(openXnat, [PETMR2_PT_4]))
        execute(new QidoTestSpec(SERIES).studyInstanceUID(PETCT2.studyInstanceUID).contentType(contentType).expectedResultIfGuestAllowed(openXnat, [PETCT2_CT_2]))
    }

    private void performAuthenticatedStudiesQuery(QidoRequestContentType contentType, User user, Collection<DicomWebTestStudy> expectedResult) {
        execute(new QidoTestSpec(STUDIES).contentType(contentType).user(user).simpleQuery(AccessionNumber, '*').expectedResult(expectedResult))
    }

    private void performAuthenticatedSeriesQuery(QidoRequestContentType contentType, User user, Collection<DicomWebTestSeries> expectedResult) {
        execute(new QidoTestSpec(SERIES).contentType(contentType).user(user).simpleQuery(Modality, '*').expectedResult(expectedResult))
    }

    private void performScopedAuthenticatedSeriesQuery(QidoRequestContentType contentType, User user, DicomWebTestStudy studyForUid, Collection<DicomWebTestSeries> expectedResult) {
        execute(new QidoTestSpec(SERIES).studyInstanceUID(studyForUid).contentType(contentType).user(user).expectedResult(expectedResult))
    }

    private void performGuestStudyRetrieveTest(WadoRequestContentType contentType, boolean openXnat) {
        execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID(CT3).contentType(contentType).expectedResultIfGuestAllowed(openXnat, [CT3]))
        [PETMR4, CT2].each { inaccessibleStudyForGuest ->
            execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID(inaccessibleStudyForGuest).contentType(contentType).expectedStatusCode(openXnat ? 404 : 401))
        }
    }

    private void performGuestSplitStudyRetrieveTest(WadoRequestContentType contentType, boolean openXnat) {
        [PETMR2_PT, PETCT2_CT].each { partialStudy ->
            execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID(partialStudy).contentType(contentType).expectedResultIfGuestAllowed(openXnat, [partialStudy]))
        }
    }

    private void performGuestSeriesRetrieveTest(WadoRequestContentType contentType, boolean openXnat) {
        publicSeries.each { accessibleSeries ->
            execute(new WadoTestSpec(WadoRequestType.SERIES).seriesInstanceUID(accessibleSeries).contentType(contentType).expectedResultIfGuestAllowed(openXnat, [accessibleSeries]))
        }
    }

    private void performGuestInaccessibleSeriesRetrieveTest(WadoRequestContentType contentType, boolean openXnat) {
        (allSeries - publicSeries).each { series ->
            execute(new WadoTestSpec(WadoRequestType.SERIES).seriesInstanceUID(series).contentType(contentType).expectedStatusCode(openXnat ? 404 : 401))
        }
    }

    private void performGuestInstanceRetrieveTest(WadoRequestContentType contentType, boolean openXnat) {
        publicInstances.each { instance ->
            execute(new WadoTestSpec(WadoRequestType.INSTANCE).sopInstanceUID(instance).contentType(contentType).expectedResultIfGuestAllowed(openXnat, [instance]))
        }
    }

    private void performGuestInaccessibleInstanceRetrieveTest(WadoRequestContentType contentType, boolean openXnat) {
        (allInstances - publicInstances).each { instance ->
            execute(new WadoTestSpec(WadoRequestType.INSTANCE).sopInstanceUID(instance).contentType(contentType).expectedStatusCode(openXnat ? 404 : 401))
        }
    }
    
    private void performAuthenticatedStudyRetrieveTest(WadoRequestContentType contentType, User user, Collection<DicomWebTestStudy> studies) {
        studies.each { study ->
            execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID(study).contentType(contentType).user(user).expectedResult(study))
        }
    }
    
    private void performAuthenticatedInaccessibleStudyRetrieveTest(WadoRequestContentType contentType, User user, Collection<DicomWebTestStudy> studies) {
        studies.each { study ->
            execute(new WadoTestSpec(WadoRequestType.STUDY).studyInstanceUID(study).contentType(contentType).user(user).expectedStatusCode(404))
        }
    }
    
    private void performAuthenticatedSeriesRetrieveTest(WadoRequestContentType contentType, User user, Collection<DicomWebTestSeries> seriesCollection) {
        seriesCollection.each { series ->
            execute(new WadoTestSpec(WadoRequestType.SERIES).seriesInstanceUID(series).contentType(contentType).user(user).expectedResult(series))
        }
    }

    private void performAuthenticatedInaccessibleSeriesRetrieveTest(WadoRequestContentType contentType, User user, Collection<DicomWebTestSeries> seriesCollection) {
        seriesCollection.each { series ->
            execute(new WadoTestSpec(WadoRequestType.SERIES).seriesInstanceUID(series).contentType(contentType).user(user).expectedStatusCode(404))
        }
    }

    private void performAuthenticatedInstanceRetrieveTest(WadoRequestContentType contentType, User user, Collection<DicomWebTestInstance> instances) {
        instances.each { instance ->
            execute(new WadoTestSpec(WadoRequestType.INSTANCE).sopInstanceUID(instance).contentType(contentType).user(user).expectedResult(instance))
        }
    }

    private void performAuthenticatedInaccessibleInstanceRetrieveTest(WadoRequestContentType contentType, User user, Collection<DicomWebTestInstance> instances) {
        instances.each { instance ->
            execute(new WadoTestSpec(WadoRequestType.INSTANCE).sopInstanceUID(instance).contentType(contentType).user(user).expectedStatusCode(404))
        }
    }

}
