package org.nrg.xnat.dicom.test.unit

import com.fasterxml.jackson.databind.ObjectMapper
import org.apache.commons.io.FileUtils
import org.nrg.xnat.dicom.jackson.module.JsonDicomWebDeserializationModule
import org.nrg.xnat.dicom.model.DicomAttribute
import org.nrg.xnat.dicom.model.DicomObject
import org.nrg.xnat.dicom.model.DicomPersonNames
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

import static org.junit.Assert.assertEquals
import static org.dcm4che3.data.VR.*

class TestJsonDeserialization {

    private final ObjectMapper objectMapper = new ObjectMapper()

    @BeforeClass
    void registerModule() {
        objectMapper.registerModule(JsonDicomWebDeserializationModule.build())
    }

    @Test
    void testBasicDeserialization() throws IOException {
        final DicomObject root = new DicomObject()
        final List<DicomAttribute> tags = root.getAttributes()

        tags.add(DicomAttribute.ofSingleton('ISO_IR192').vr(CS).tag('00080005'))
        tags.add(DicomAttribute.ofSingleton('20130409').vr(DT).tag('00080020'))
        tags.add(DicomAttribute.ofSingleton('131600.0000').vr(TM).tag('00080030'))
        tags.add(DicomAttribute.ofSingleton('11235813').vr(SH).tag('00080050'))
        tags.add(DicomAttribute.ofStrings('CT', 'PET').vr(CS).tag('00080061'))
        tags.add(DicomAttribute.ofSingleton(new DicomPersonNames().alphabetic('^Bob^^Dr.')).vr(PN).tag('00080090'))
        tags.add(DicomAttribute.ofSingleton('Vendor A').vr(LO).tag('00090010'))
        tags.add(DicomAttribute.ofSingleton('z0x9c8v7').vr(UN).tag('00091002'))
        tags.add(DicomAttribute.ofSingleton(new DicomPersonNames().alphabetic('Wang^XiaoDong').ideographic('王^小東')).vr(PN).tag('00100010'))

        final DicomObject sequenceItem0 = new DicomObject()
        final DicomObject sequenceItem1 = new DicomObject()
        sequenceItem0.getAttributes().add(DicomAttribute.ofSingleton('54321').vr(LO).tag('00100020'))
        sequenceItem0.getAttributes().add(DicomAttribute.ofSingleton('Hospital B').vr(LO).tag('00100021'))
        sequenceItem1.getAttributes().add(DicomAttribute.ofSingleton('24680').vr(LO).tag('00100020'))
        sequenceItem1.getAttributes().add(DicomAttribute.ofSingleton('Hospital C').vr(LO).tag('00100021'))
        tags.add(DicomAttribute.ofSequenceItems(sequenceItem0, sequenceItem1).vr(SQ).tag('00101002'))
        tags.add(DicomAttribute.ofSingleton(4).vr(IS).tag('00201206'))

        assertEquals(root, objectMapper.readValue(FileUtils.getFile('src', 'test', 'resources', 'test.json'), DicomObject.class))
    }

}
