package org.nrg.xnat.dicom.test.unit

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import org.apache.commons.io.FileUtils
import org.nrg.xnat.dicom.jackson.module.XmlXnatDicomWebRestDeserializers
import org.nrg.xnat.dicom.model.DicomAttribute
import org.nrg.xnat.dicom.model.DicomObject
import org.nrg.xnat.dicom.model.DicomPersonNames
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

import static org.dcm4che3.data.VR.*
import static org.junit.Assert.assertEquals

class TestXmlDeserialization {

    private final ObjectMapper objectMapper = new XmlMapper()

    @BeforeClass
    void registerModule() {
        objectMapper.registerModule(XmlXnatDicomWebRestDeserializers.build())
    }

    @Test
    void testBasicDeserialization() throws IOException {
        final DicomObject root = new DicomObject()
        final List<DicomAttribute> tags = root.getAttributes()

        tags.add(DicomAttribute.ofSingleton('BRAIN').vr(LO).tag('00081030').keyword('StudyDescription'))
        tags.add(DicomAttribute.ofPersonNames(
                new DicomPersonNames().alphabetic('Yamada^Tarou').ideographic('山田^太郎').phonetic('やまだ^たろう'),
                new DicomPersonNames().alphabetic('Alias')
        ).tag('00101001').keyword('OtherPatientNames'))
        tags.add(DicomAttribute.ofStrings('Version 1.0.0', 'Version 3').vr(LO).tag('00181020').keyword('SoftwareVersions'))
        final DicomObject sequenceItem = new DicomObject()
        sequenceItem.getAttributes().add(DicomAttribute.ofSingleton('HOSPITAL').vr(LO).tag('00080080').keyword('InstitutionName'))
        tags.add(DicomAttribute.ofSequenceItems(sequenceItem).tag('00080051').keyword('IssuerOfAccessionNumberSequence'))

        assertEquals(root, objectMapper.readValue(FileUtils.getFile('src', 'test', 'resources', 'test.xml'), DicomObject.class))
    }

    @Test
    void testEmptyValuesAndItems() throws IOException {
        final DicomObject root = new DicomObject()
        final List<DicomAttribute> tags = root.getAttributes()

        tags.add(DicomAttribute.ofStrings('An Issue', null, 'Another Issue').vr(LO).tag('00081080').keyword('AdmittingDiagnosesDescription'))
        final DicomObject sequenceItem = new DicomObject()
        sequenceItem.getAttributes().add(DicomAttribute.ofSingleton('HOSPITAL').vr(LO).tag('00080080').keyword('InstitutionName'))
        tags.add(DicomAttribute.ofSequenceItems(new DicomObject(), sequenceItem).tag('00080051').keyword('IssuerOfAccessionNumberSequence'))

        assertEquals(root, objectMapper.readValue(FileUtils.getFile('src', 'test', 'resources', 'empty_things.xml'), DicomObject.class))
    }

}
