package org.nrg.xnat.dicom.test.unit

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import org.dcm4che3.data.VR
import org.nrg.xnat.dicom.jackson.module.JsonDicomWebSerializationModule
import org.nrg.xnat.dicom.model.BulkData
import org.nrg.xnat.dicom.model.DicomAttribute
import org.nrg.xnat.dicom.model.DicomObject
import org.nrg.xnat.dicom.model.DicomPersonNames
import org.testng.annotations.BeforeClass
import org.testng.annotations.Test

import static org.junit.Assert.assertEquals

class TestJsonSerialization {

    private final static ObjectMapper objectMapper = new ObjectMapper()

    @BeforeClass
    void registerModule() {
        objectMapper.registerModule(JsonDicomWebSerializationModule.build())
    }

    @Test
    void testBasicSerialization() throws JsonProcessingException {
        final DicomObject root = new DicomObject()
        final DicomObject sequenceItem = new DicomObject()
        sequenceItem.getAttributes().add(DicomAttribute.ofSingleton('54321').vr(VR.LO).tag('00100020'))
        root.getAttributes().add(DicomAttribute.ofSingleton(1).vr(VR.IS).tag('00200011'))
        root.getAttributes().add(DicomAttribute.ofSingleton(new DicomPersonNames().alphabetic('TEST')).vr(VR.PN).tag('00100010'))
        root.getAttributes().add(DicomAttribute.ofSingleton(sequenceItem).vr(VR.SQ).tag('00101002'))
        assertEquals(
                '{"00200011":{"vr":"IS","Value":[1]},"00100010":{"vr":"PN","Value":[{"Alphabetic":"TEST"}]},"00101002":{"vr":"SQ","Value":[{"00100020":{"vr":"LO","Value":["54321"]}}]}}',
                objectMapper.writeValueAsString(root)
        )
    }

    @Test // Tests PS3.18 F.2.5 (empty values as null and empty sequence items as empty JSON objects)
    void testModelNullValues() throws JsonProcessingException {
        final DicomObject root = new DicomObject()
        root.getAttributes().add(DicomAttribute.ofStrings('v1.0', null, 'v2.0').vr(VR.LO).tag('00181020'))
        root.getAttributes().add(DicomAttribute.ofSingleton(new DicomObject()).vr(VR.SQ).tag('00101002'))
        assertEquals('{"00181020":{"vr":"LO","Value":["v1.0",null,"v2.0"]},"00101002":{"vr":"SQ","Value":[{}]}}', objectMapper.writeValueAsString(root))
    }

    @Test
    void testBulkDataSerialization() throws JsonProcessingException {
        final DicomObject root = new DicomObject()
        root.getAttributes().add(DicomAttribute.ofVR(VR.OB).tag('7FE00010').bulkData(new BulkData().uri('http://google.com')))
        assertEquals('{"7FE00010":{"vr":"OB","BulkDataURI":"http://google.com"}}', objectMapper.writeValueAsString(root))
    }

}
